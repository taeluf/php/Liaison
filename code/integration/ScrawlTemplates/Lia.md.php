<?php
/**
 * Main Liaison Server Documentation Template.
 * Params are in the `$args` array.
 *
 * @param $lia \Lia instance to document.
 */
?>
# Your Server
Liaison servers are composed of shared addons, methods, hooks, and properties. The built-in addons provide routing, views, cache, theming, css & js file management. 

See the below docs for a full overview of your Liaison setup. 

- [Methods](Lia/Methods.md)
- [Routes](Lia/Routes.md)


## TODO
What do I want to know?
- DONE? Liaison Methods
- DONE? routes (could be cleaner & have more information, and there is a bug)
- apps / packages
- addons
- views
- phad ... stuff (but maybe that doesn't go here)

