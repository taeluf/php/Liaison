<?php
/**
 * Documentation of Liaison Methods
 * Params are in the `$args` array.
 *
 * @param $lia \Lia instance to document.
 */
$lia = $args['lia'];
$methods = $lia->methods;

$grouped_methods = ['global-function'=>[], 'anonymous-function'=>[]];

require_once(__DIR__.'/../functions.php');

foreach ($methods as $m=>$callable){
    if (is_array($callable)){
        $obj = $callable[0];
        $method = $callable[1];
        $callable_description = '';
        if (is_object($obj)){
            $class = get_class($obj);
            $info = [
                'class'=>$class,
                'method'=>$method,
                'arg_list'=>get_arg_list($this,$class, $method),
                'description'=>get_description($this, $class, $method),
            ];
            // $class = get_class($obj);
            // $description = "Calls `$class->$method()`";
        }  else {
            $info = [
                'class'=>$obj,
                'static_method'=>$method,
                'arg_list'=>get_arg_list($this,$obj, $method),
            ];
        }

        $grouped_methods[$class][$m] = $info;
    } else if (is_string($callable)){
        $grouped_methods['global-function'][$m] = $callable;
    } else {
        $grouped_methods['anonymous-function'][$m] = $callable;
    }

}

?>
# Global Liaison Methods
Below is a list of `$lia->methods()` grouped by the classes those methods map to. 

## Global functions
These functions are defined globally and can be called from anywhere. They can be called through liaison, as it is on the left.
<?php
foreach ($grouped_methods['global-function'] as $lia_method=>$callable){
    $refMethod = new ReflectionFunction($callable);
    $full = $refMethod->getFileName();
    $file = get_friendly_path($full, $this);

    // exit;

    // $root = $this->dir_root;
    // var_dump("Rel: $rel");
    // $lia->dump(get_object_vars($this));
    // exit;

    echo "- `$lia_method(...)` - Calls `$callable(...)`, defined in $file\n";
}
?>

## Anonymous functions
These are anonymous functions, added to Liaison. 
<?php
foreach ($grouped_methods['anonymous-function'] as $lia_method=>$callable){
    $refMethod = new ReflectionFunction($callable);
    $full = $refMethod->getFileName();
    $file = get_friendly_path($full, $this);

    echo "- `$lia_method(...)` - Calls anonymous function, defined in $file  \n";
}
?>

<?php

foreach ($grouped_methods as $group=>$list){
    if (!class_exists($group,true))continue; // I'll handle anon & global functions separately

    $parts = explode('\\',$group);
    $class_var = '$'.lcfirst(array_pop($parts));

    echo "\n## $group\n";
    echo get_class_description($this, $group)."  \n";

    foreach ($list as $lia_method => $info){
        $class_method = $info['method'];
        $class_name = $info['class'];
        $arg_list = $info['arg_list'];
        $description = $info['description'];
        echo "- `$lia_method($arg_list)` - $description. See `$class_var->$class_method()`  \n";
    }

}
?>
