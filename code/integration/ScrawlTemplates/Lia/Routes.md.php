<?php
/**
 * Documentation of Liaison routes
 * Params are in the `$args` array.
 *
 * @param $lia \Lia instance to document.
 */

$lia = $args['lia'];

require_once(__DIR__.'/../functions.php');


$router = $lia->addon('lia:server.router');
// $lia->dump($router->routeMap);
// $lia->dump();

?>
# GET routes
<?php
    $clean_routes = get_clean_routes($router->routeMap['GET'] ?? [], $this);
    ksort($clean_routes);
    foreach ($clean_routes as $pattern => $target){
        echo "- `$pattern`: $target \n";
    }

?>


# POST routes
<?php
    $clean_routes = get_clean_routes($router->routeMap['POST'] ?? [], $this);
    ksort($clean_routes);
    foreach ($clean_routes as $pattern => $target){
        echo "- `$pattern`: $target \n";
    }
?>
