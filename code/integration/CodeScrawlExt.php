<?php

namespace Lia\Integration;

use Tlf\Scrawl\DoNothingExtension;

class CodeScrawlExt extends DoNothingExtension {

    public function bootstrap(){
        $this->scrawl->template_dirs[] = __DIR__.'/ScrawlTemplates/';
    }

    public function scrawl_finished(){
        $_SERVER['REQUEST_URI'] = '/';
        // print_r($this->scrawl->options);
        $init_script = $this->scrawl->options['Lia.init_script'];
        $path = $this->scrawl->options['dir.root'] . '/' . $init_script;
        // $out_file = $this->scrawl->options['Lia.doc_path']

        $lia = require($init_script);


        $this->scrawl->write_doc('Lia.md', 
            $this->scrawl->get_template('Lia', ['lia'=>$lia])
        );

        $this->scrawl->write_doc('Lia/Routes.md', 
            $this->scrawl->get_template('Lia/Routes', ['lia'=>$lia])
        );
    }
}
