<?php

namespace Lia\Addon;

/**
 *
 * @note addon_name is 'goto'
 */
class Redirect extends \Lia\Addon {

    public string $fqn = 'lia:server.redirect';
    
    public $redirectUrl;
    // public $withContent = false;
    public $withMessage = false;
    public $statusCode = 302;
    public $message = null;

    // public $name = 'redirect';

    public function init_lia(){
        $lia = $this->lia;
    }

    public function goto($url, $statusCode=302){
        $this->redirectUrl = $url;
        $this->statusCode = $statusCode;
        // $this->withContent = false;
        $this->withMessage = false;
        $this->go();
    }
    public function gotoSetMessage($message){
        $this->message = $message;
        $this->withMessage = true;
        // $this->withContent = false;
    }
    
    public function gotoWithMessage($url, $message, $statusCode=302){
        $this->redirectUrl = $url;
        // $this->withContent = false;
        $this->withMessage = true;
        $this->message = $message;
        $this->statusCode = $statusCode;
        $this->go();
    }
    
    /**
     * Show the content of this request at the given url
     * Your target route must check for a goto message with getGotoMessage()
     */
    // public function gotoWithContent($url, $statusCode=302){
    //     $this->redirectUrl = $url;
    //     $this->withContent = true;
    //     $this->statusCode = $statusCode;
    //     $this->withMessage = false;
    // }
    
    /**
     * Get the redirect message associated with `$_GET['message_id']`
     *   
     * @return string|false
     */
    public function getGotoMessage() {
        $id = $_GET['message_id']??null;
        if ($id===null)return false;
        $message = \Lia\Addon\Cache::from($this->lia)->get_cache_file_content('lia.goto.withMessage.'.$id);
        return $message;
    }

    /**
     * get the url with `message_id` set in query params.
     * Will cache redirect message to a file if one is present
     *
     * @return a url or null
     */
    public function get_url() {
        $url = $this->redirectUrl;
        if ($url == null) return null;

        if ($this->withMessage){
            $id = uniqid().'-'.uniqid();
            $message = $this->message ?? "Redirect: Unknown Message";
            \Lia\Addon\Cache::from($this->lia)->cache_file('lia.goto.withMessage.'.$id, $message, 300);

            $path = parse_url($url, PHP_URL_PATH);
            $query = parse_url($url, PHP_URL_QUERY);
            if ($query===null)$query = '';
            $qps = [];
            parse_str($query, $qps);
            $qps['message_id'] = $id;

            $url = $path.'?'.http_build_query($qps);

        }

        return $url;
    }

    public function go(){
        if ($this->redirectUrl=='')return;

        $url = $this->get_url();

        // $this->empty_buffer();
        ob_start();

        header("Location: {$url}", true, $this->statusCode ?? 302);

    }

    // public function setHeadersNow($route, $response){
    //     if ($this->redirectUrl=='')return;
    //     $response->useTheme = false;
    //     $response->sendContent = false;
    //     $id = uniqid().'-'.uniqid();
    //     $message =
    //         $this->withMessage
    //             ? $this->message
    //             : ($this->withContent ? $response->content : false)
    //             ;
    //
    //     $url = $this->redirectUrl;
    //     if ($message!==false){
    //         //cache the message for 20 seconds
    //         \Lia\Addon\Cache::from($this->lia)->cache_file('lia.Redirect.withMessage.'.$id, $message, 20);
    //
    //         $path = parse_url($url, PHP_URL_PATH);
    //         $query = parse_url($url, PHP_URL_QUERY);
    //         $qps = [];
    //         parse_str($query, $qps);
    //         $qps['message_id'] = $id;
    //
    //         $url = $path.'?'.http_build_query($qps);
    //     }
    //     $response->addHeader(["Location: {$url}", $this->statusCode ?? 302]);
    // }
    
    /**
     * Set `Location: $gotoUrl` header to $response. 
     *
     */
    // public function onEmitRouteResolved($route, $response){
    //     $this->setHeadersNow($route, $response);
    // }
}
