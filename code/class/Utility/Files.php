<?php

namespace Lia\Utility;

/**
 * Utility class to work with files & directories
 * @todo move to my util repo & depend upon this class.
 */
class Files {

    /**
     * Get all files in a directory. Does not return directories
     *
     * @param $dir the directory to search in
     * @param $fPathsRelTo To get relative paths, this is removed from the beginning of each file path returned
     * @param $endingWith only get files that end with the given string, like `.php` or `.md`, etc
     * @return array of files
     */
    static public function all($dir,$fPathsRelTo='', $endingWith=''){
        if (!is_dir($dir))return [];
        $dir = str_replace(['///','//'],'/','/'.$dir.'/');
        $fPathsRelTo = $fPathsRelTo ? str_replace(['///','//'],'/','/'.$fPathsRelTo.'/') : null;

        //echo "\n\n\nDir: ".$dir;
        //echo "\nfPathsRelTo: ".$fPathsRelTo;
        //echo "\nEnding With: ".$endingWith;

        $files = [];
        self::load_all($dir, $fPathsRelTo??'', $endingWith??'',$files);
        return $files;

        //return self::get_all_2($dir, $fPathsRelTo??'', $endingWith??'');

        //return $files;
    }

    /** optimized version of the function without string replaces */
    static protected function load_all(string $dir, string $fPathsRelTo,string $endingWith, array &$files = []){
        $dh = opendir($dir);

        $len = strlen($endingWith);
        $parent_len = strlen($fPathsRelTo);
        while ($file=readdir($dh)){
            if ($file=='.'||$file=='..')continue;
            if (!is_dir($dp=$dir.'/'.$file)){
                $path = substr(
                    $dp,
                    $parent_len
                );
                if ($endingWith!=''
                    &&substr($path,-$len)!=$endingWith)continue;
                $files[] = $path;
                continue;
            }
            self::load_all($dp,$fPathsRelTo,$endingWith,$files);
        }
        closedir($dh);
    }

}
