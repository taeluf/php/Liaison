<?php

namespace Lia;


/**
 * A class to organize all hook names across the main package.
 * This class started on Aug 22, 2024. Pre-existing hooks may get added into here, but I have no explicit plans to do that.
 * New hooks hopefully will all be added here.
 */
class Hooks {

    /**
     * Registered hooks should return an array of patterns like `array<string rel_file_name, string url>`. 
     *
     * @example function receive_hook(Lia\Package\Server $package_routes_are_loaded_from, array $array_of_patterns): array { return $array_of_patterns;}
     */
    const PACKAGE_ROUTE_PATTERNS_LOADED = 'lia:package.route_patterns_loaded';


    /**
     * Called after all packages are ready, before routes are selected or filtered. 
     * 
     * method signature: junction(\Lia\Obj\Request $request, \Lia\Obj\Response $response): void;
     *
     * This is a good time to add extra routes in special circumstances. 
     *
     * For example, to add an extra layer of protection to admin routes, you should only add them if the logged-in-user is already verified as an admin. But user-role checks require database access, so this adds extra computation. Therefore, you might use REQUEST_STARTED hook to check if the request is to `/admin/...` and ONLY then check the user role and add the admin routes.
     * 
     *
     * @deprecated string value 'RequestStarted' will likely be removed at some point and replaced with `lia:server.request_started`. This const var name will likely remain the same.
     */
    const REQUEST_STARTED = 'RequestStarted';


    /**
     * Called after a view's content has been loaded. Provides an opportunity to modify this content.
     * This only supports having one registered handler. More than one registered will throw exception 
     *
     * @param $view is a file path or a callable. Callable may be an anonymous function or an array (`[$obj, 'method']`) or string 'some_function'.
     * @return string content The original content will be replaced with whatever the hook handler returns
     * function handle_view_loaded(?string $namespace, string $view_name, array $view, string $content): string
     *
     */
    const VIEW_LOADED = 'lia:server.view_loaded';


    /**
     * Called after the list of possible routes is reduced to one route, before the route is processed (before page content is rendered).
     * 
     * You cannot modify the route, but you can throw an exception or `exit`, or check user access. 
     *
     * @param $route the requested url, params, and target on the server
     * function handle_routes_filtered(\Lia\Obj\Route $route): void {}
     *
     * @deprecated string value 'RoutesFiltered' will likely be removed at some point and replaced with `lia:server.routes_filtered`. This const var name will likely remain the same.
     */
    const ROUTES_FILTERED = 'RoutesFiltered';

    /**
     * Called after a route is processed & page content is rendered, before the theme is applied. You can modify the response headers and content.
     *
     * @param $route the requested url, params, and target on the server
     * @param $response HTTP Headers and response content (theme has not been applied yet).
     * function handle_route_resolved(\Lia\Obj\Route $route, \Lia\Obj\Response $response): void {}
     *
     *name:
     * @deprecated string value 'RoutesResolved' will likely be removed at some point and replaced with `lia:server.routes_resolved`. This const var name will likely remain the same.
     */
    const ROUTE_RESOLVED = 'RouteResolved';

    /** 
     * Called after the list of matching routes is loaded. The list of routes can NOT be modified here. Use `FILTER_ROUTE` for that.
     *
     * @param $routeList array<int index, \Lia\Obj\Route route> array of matching routes
     * @example function handle_route_found(array $routeList): void {}
     *
     * @deprecated string value will change, const will stay the same
     */
    const ROUTES_FOUND = 'RoutesFound';


    /** 
     * Called once for each matching route. If **any** FILTER_ROUTE hook returns boolean `false`, then the passed route will be removed from the routes list. A null or falsey return value will do nothing. Only strict `false` has any effect. This is called regardless the size of the route list.
     *
     * @param $route \Lia\Obj\Route the route being checked for filtering
     * @param $routeList array<int index, \Lia\Obj\Route route> array of matching routes
     * @return false to remove a route from the list. 
     * @example 
       function handle_filter_route(\Lia\Obj\Route $route, array $routeList): bool 
       { 
           if (is_bad_route($route)) return false; 
           else return true;
       } 
     *
     * @deprecated string value will change, const will stay the same
     */
    const FILTER_ROUTE = 'FilterRoute';




    /**
     * Called after a response is fully processed, just before it is sent.
     *
     * @param $response \Lia\Obj\Response
     * @return void
     * @example
     * function onResponseReady(\Lia\Obj\Response $response){
     *     $response->content = modify_response_content_i_guess($response);
     * }
     *
     * @deprecated string value will change, const will stay the same.
     */
    const RESPONSE_READY = 'ResponseReady';

}

