<?php

class Lia {

    /**
     * array of addons with namespaces, like 'lia:server.router'
     */
    public $addons = [];

    /** packages where key is fqn, like 'lia:server' */
    public $packages = [];

    public function __construct(){

    }


    public function addPackage(object $package, string $fqn){
        $this->packages[$fqn] = $package;
    }

    /**
     * Add an addon by it's fully qualified name
     * @param $addon, generally a `\Lia\Addon`, but can be any object
     * @param $fqn the fully qualified name like `namespace:package.addon_name`
     */
    public function addAddon(object $addon, string $fqn){
        $this->addons[$fqn] = $addon;
    }



    /**
     * Get an addon by it's fully-qualified-name
     * @param $fqn string like 'lia:server.router'
     */
    public function addon($fqn): \Lia\Addon {
        if (!isset($this->addons[$fqn])){
            throw new \Lia\Exception(\Lia\Exception::ADDON_NOT_SET,$fqn,implode(", ", array_keys($this->addons)));
        }
        return $this->addons[$fqn];
    }
    /**
     * Get a package by it's fully-qualified-name
     * @param $fqn string like 'lia:server'
     */
    public function package($fqn): \Lia\Package {
        if (!isset($this->fqn_packages[$fqn])){
            throw new \Lia\Exception(\Lia\Exception::PACKAGE_NOT_SET,$fqn,implode(", ", array_keys($this->packages)));
        }
        return $this->packages[$fqn];
    }

    public function dump_thing($thing){
        $thing = $this->__map_array__($thing);
        print_r($thing);
        unset($thing);
    }

    /**
     * Dump a bunch of info about liaison. Methods. Addons. Properties.
     * @param $thing a variable to dump or null to dump liaison fully
     */
    public function dump($thing=null){
        $c = [$this, '__map_array__'];
        if ($thing!=null){
            print_r($c($thing));
            return;
        }

        echo "\n\n\n-----------\nAddons:\n";
        $addons = array_map($c, $this->addons);
        print_r($addons);
        unset($addons);

        echo "\n\n\n-----------\nPackage Properties Tree:\n";
        $package_props = [];
        $addon_props = [];
        foreach ($this->packages as $k=>$p){
            $package_props[$k] = array_map($c,get_object_vars($p));
            foreach ($p->addons as $addon_key => $addon){
                $addon_props[$k][$addon_key] = array_map($c,get_object_vars($addon));
            }
        } 
        print_r($package_props);
        unset($package_props);

        echo "\n\n\n-----------\nAddon Properties Tree:\n";
        print_r($addon_props);
        unset($addon_props);

    }

    public function __map_array__($value){
        if (is_callable($value)&&is_array($value)
            &&is_object($value[0]))return get_class($value[0]).'#'.spl_object_id($value[0]).'->'.$value[1];
        if (is_object($value))return get_class($value).'#'.spl_object_id($value);
        if (is_array($value))return array_map([$this, '__map_array__'], $value);

        return $value;
    }

}
