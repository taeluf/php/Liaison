<?php

namespace Lia;


class Exception extends \Exception {

    const VIEW_NOT_FOUND = "View '%s' has not been added to Liaison.";
    const CANNOT_COPY_NONEXISTENT_VIEW_NS = "Cannot change '%s' view namespace to '%s', because the original namespace does not exist.";
    const CANNOT_OVERWRITE_EXISTING_VIEW = "Cannot override view '%s' with the same-named view from namespace '%s'";
    const ONLY_ONE_VIEW_LOADED_HOOK_ALLOWED = "Only one view loaded hook is allowed.";
    const REQUEST_TARGET_NOT_HANDLED = "Url %s was requested and matched a route, but target could not be resolved. \nTarget is `%s`";
    const ADDON_NOT_SET = "Addon with fully qualified name '%s' has not been added. Available addons are: %s";
    const PACKAGE_NOT_SET = "Package with fully qualified name '%s' has not been added. Available packages are: %s";


    /**
     * @param $exception_message should be a message named on this class
     * @param $strings as will be passed to sprintf()
     */
    public function __construct(string $exception_message, ...$strings){
        $full_message = sprintf($exception_message,...$strings);
        parent::__construct($full_message);
    }

}
