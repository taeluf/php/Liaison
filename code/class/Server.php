<?php

namespace Lia\Package;

/**
 *
 * @test loading of config.json
 */
#[\AllowDynamicProperties]
class Server extends \Lia\Package {

    public string $fqn = 'lia:server';

    public string $name = 'server';

    /** key=>value params to automatically `extract()` into public files. Each package has it's own list of params to extract, so params from one package will not be shared with public files in other packages. */
    public array $public_file_params = [];

    /**
     * Setting this directly does not work, bc routes are setup during the constructor.
     *
     * Instead do `$lia->set('package_name.base_url', '/base_url/');`
     */
    public $base_url = '';

    /**
     * @param $path the url path within the package
     * @return url with the base_url prepended
     */
    public function url(string $path){
        return str_replace(
            '//', '/',
            $this->base_url.$path
        );
    }

    public function __construct($lia, $name='lia:server', $dir=null, $base_url=null){
        parent::__construct($lia, $name, $dir);

        // set params from configs
        $this->base_url = $base_url ?? $this->config('base_url');
        if ( ($fqn = $this->config('fqn')) != null){
            $this->fqn = $fqn;
        }
        if ( ($pfp = $this->config('public_file_params')) != null){
            $this->public_file_params = array_merge_recursive($this->public_file_params, $pfp);
        }


        if (!is_null($base_url)){
            $this->base_url = $base_url;
        }

        $this->dir = $dir ?? dirname(__DIR__);
        $this->load_addons('addon');

        $cache = \Lia\Addon\Cache::from($lia);
        if (!isset($cache->dir))$cache->dir = $this->dir('cache');

        // var_dump($this->dir);
        // exit;
        $public_dir = $this->dir.'/public/';
        $patterns = \Lia\Addon\Router::from($lia)
                         ->dir_to_patterns($public_dir);

        $results = \Lia\Addon\Hook::from($lia)->call(\Lia\Hooks::PACKAGE_ROUTE_PATTERNS_LOADED, $this, $patterns);
        $num_results = count($results);
        if ($num_results == 1){
            $patterns = $results[0];
        } else if ($num_results > 1){
            throw new \Exception("Only one hook may be registered to '".\Lia\Hooks::PACKAGE_ROUTE_PATTERNS_LOADED."'. ".count($results)." were registered.");
        } // else zero, do nothing

        // var_dump($patterns);

        // var_dump($dir);
        // print_r($patterns);
        foreach ($patterns as $file=>$pattern){
            $full_pattern = $this->base_url.$pattern;

            // echo "\nPattern: $full_pattern";
            // echo "\n   File: $public_dir/$file";
            \Lia\Addon\Router::from($lia)->addRoute($full_pattern,$public_dir.'/'.$file, $this);
        }
        // echo "\n\n\n-----------\n\n";
        // echo "\n\n\n-----------\n\n";

        $view_dir = $this->dir.'/view/';
        if (is_dir($view_dir)){
            \Lia\Addon\View::from($lia)->addDir($view_dir, $this);
        }


        // @NOTE I'm adding dir `/theme/` as a view dir in Server package setup, and I don't know why. I should remove this probably.
        $theme_dir = $this->dir.'/theme/';
        if (is_dir($theme_dir)){
            \Lia\Addon\View::from($lia)->addDir($theme_dir, $this);
        }

        // echo 'pre init';
        $this->init_lia();
        // echo 'READYhere';
        // exit;
        $this->ready();
    }

    // @NOTE old 'setup_public_routes()' method has been disabled. It had an error & doesn't appear to be used
    //public function setup_public_routes($dir, $prefix){
        //$patterns = $this->lia->fqn_addons['lia:server.router']
                         //->dir_to_patterns($dir);

        //foreach ($patterns as $file=>$pattern){
            //\Lia\Addon\Router::from($lia)->addRoute($prefix.$pattern,$public_dir.'/'.$file, $this);
        //}
    //}

    static public function main($lia){
        return new static($lia, 'server', dirname(__DIR__));
    }
    static public function main_dir(){
        return dirname(__DIR__);
    }

    public function goto($url){
        $url = $this->url($url);
        //$url = $this->base_url.'/'.$url;
        //$url = str_replace(['///','//'], '/', $url);
        \Lia\Addon\Redirect::from($this->lia)->goto($url);
        // $this->lia->dump();
    }

}
