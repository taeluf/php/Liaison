<?php

namespace Lia\Obj;

/**
 * An un-opinionated View interface, used by the View component
 *
 */
interface IView {

    /**
     * @param $name Relative path to the view without file extension. Ex: `view/theme/Header.php` is named `theme/Header`
     * @param $dir The root directory for the view
     * @param $args that should be passed to a template file
     */
    public function __construct($name, $dir, $args);
}
