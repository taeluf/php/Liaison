<?php

namespace Lia\Obj;

/**
 * A View, which passes properties to a template file and loads associated resources (.css/.js files). You must invoke `__toString()` (`''.$view`) or `$view->content()` or `$view->resources()` or nothing happens.
 *
 * @todo Add an init + template approach like `view/Blog/template.php` and `view/Blog/init.php`
 * @todo Add automatic routing via the view system (which is more robust and neat than simple public-files), such as with a `public-view` dir 
 *     @todo a `<lia-route pattern="/"> might be a valid option, but that seems more like an extension feature than a core feature
 * @todo Add events for before-view & after-view
 *
 * @todo remove dependency upon $lia and instead do liaison stuff in the addon
 * @todo remove addResourceFile() call from this class & do it in the view addon instead, using the resources array
 *
 */
class View implements IView {

    /**
     * Relative path to the view without file extension. Ex: `view/theme/Header.php` is named `theme/Header`
     * @todo add namespacing like vendorname:view/name
     */
    protected $name;
    /**
     * The root directory for finding views
     */
    protected $dir;
    /**
     * Arguments to pass to the template file
     */
    public $args = [];
    
    
    /**
     * Output from including the template file
     */
    protected $content = null;

    /**
     * Array of resources
     * ```php
     * $resources = ['path/to/script.js', 'path/to/theme.css'];
     * ```
     *
     */
    protected $resources = null;

    public $lia = null;

    public function __construct($name, $dir, $allArgs){
        $this->name = $name;
        $this->dir = $dir;
        $this->args = $allArgs;
        //@TODO perhaps $lia should be a required paramater, not hidden in an array
        // print_r($allArgs);
        // exit;
        $this->lia = $this->args['lia'];
        // exit;
    }

    /**
     * Load all css & js files in the view's directory
     *
     * @todo only descend into view/name/css/*.css & view/name/js/*.js
     * @return array of resource files `['css'=>['/path/to/file1.css'], 'js'=>[...]]`
     */
    public function resources(){
        if ($this->resources!==null)return $this->resources;
        $lia = $this->lia;
        $dir = $this->dir.'/'.$this->name;
        $js = \Lia\Utility\Files::all($dir,$dir,'.js');
        $css = \Lia\Utility\Files::all($dir,$dir,'.css');

        if (file_exists($dir.'.js')){
            $js[] = '.js';
        }
        if (file_exists($dir.'.css')){
            $css[] = '.css';
        }

        $resources = [];
        foreach ($js as $jsFile){
            $resources[$jsFile] = $dir.$jsFile;
            \Lia\Addon\Resources::from($lia)->addFile($dir.$jsFile);
        }
        foreach ($css as $cssFile){
            $resources[$cssFile] = $dir.$cssFile;
            \Lia\Addon\Resources::from($lia)->addFile($dir.$cssFile);
        }


        $this->resources = $resources;
        return $resources;
        // return [
            // 'css'=>$css,
            // 'js'=>$js,
        // ];
    }

    /**
     * Load template file
     */
    public function content(){
        if ($this->content!==null)return $this->content;
        $this->resources();

        extract($this->args);
        $allArgs = $this->args;
        ob_start();
        require($this->dir.'/'.$this->name.'.php');
        $content = ob_get_clean();
        $this->content = $content;
        return $content;
    }

    /**
     * shorthand for content()
     */
    public function __toString(){
        return $this->content();
    }

}

