<?php

namespace Lia\Obj;

/**
 * Mutable object to represent a web response
 */
class Response {

    
    /**
     * Generally a \Lia\Obj\Request
     * @todo type this property & constructor
     */
    public $request;
    /**
     * The body to send
     */
    public $content = '';
    /**
     * Whether to apply a theme or not
     */
    public $useTheme;
    /**
     * headers to send
     */
    public $headers = [];
    /**
     * Whether to send content or not. Mark false if you're sending headers to load-from-cache
     */
    public $sendContent = true;

    public function __construct($request){
        $this->request = $request;
    }

    /**
     * @param array $header Array of args you'd pass to `header()`
     * @todo More convenient way to add headers & set status codes
     */
    public function addHeader($header){
        $this->headers[] = $header;
    }

    /**
     * @see addHeader()
     * @param $headers [$headerArray, $headerArray2, ...]
     */
    public function addHeaders($headers){
        $this->headers = array_merge($this->headers, $headers);
    }

    public function sendHeaders(){
        foreach ($this->headers as $headerArgsArray){
            header(...$headerArgsArray);
        }
    }
}
