<!DOCTYPE html>
<html>
<head>
    <?=\Lia\Addon\Resources::from($lia)->getHtml();?>
</head>
<body>
    <header>
    </header>
    <main>
        <?=\Lia\Addon\View::from($lia)->view('error/header');?>
        <?=$content?>
    </main>
    <footer>
    </footer>
</body>
</html>
