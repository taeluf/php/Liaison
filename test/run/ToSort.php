<?php


namespace Lia\Test;

class ToSort extends \Tlf\Tester {

    public function testViewGlobalArgs(){
        $view = new \Lia\Addon\View();
        $view->addView('global-args', $this->file('test/input/ToSort/views/'));
        $view->globalArgs['glb_arg'] = $target = 'This is a global arg';

        $this->compare($target, $view->view('global-args'));
    }

}
