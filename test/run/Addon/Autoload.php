<?php

namespace Liaison\Test\Addon;


class Autoload extends \Tlf\Tester {


    public function testAutoloadNsClassWithPrefix(){
        $rand_name = $this->random();
        $class_name = 'some_ns\\for_you\\'.$rand_name;
        $class_code = $this->get_class_code($class_name);
        $namespaces_in_dir = 'some_ns\\for_you';

        $file = $this->al_dir().'/'.$rand_name.'.php';
        // mkdir(dirname($file), 0755, true);
        file_put_contents($file, $class_code);

        $autoloader = new \Lia\Addon\Autoload();
        $autoloader->addDir($this->al_dir(), $namespaces_in_dir);
        $autoloader->enable();

        
        $class = new $class_name();
        $this->compare($class_name, get_class($class));

        unlink($file);
    }

    public function testAutoloadNsClass(){
        $class_name = 'some_ns\\for_you\\'.$this->random();
        $class_code = $this->get_class_code($class_name);
        $file = $this->al_dir().'/'.str_replace('\\','/',$class_name).'.php';
        // mkdir(dirname($file), 0755, true);
        file_put_contents($file, $class_code);

        $autoloader = new \Lia\Addon\Autoload();
        $autoloader->addDir($this->al_dir());
        $autoloader->enable();

        
        $class = new $class_name();
        $this->compare($class_name, get_class($class));

        unlink($file);
    }

    public function testAutoloadClassAlone(){
        $class_name = $this->random();
        $class_code = $this->get_class_code($class_name);
        $file = $this->al_dir().'/'.$class_name.'.php';
        file_put_contents($file, $class_code);

        $autoloader = new \Lia\Addon\Autoload();
        $autoloader->addDir($this->al_dir());
        $autoloader->enable();

        
        $class = new $class_name();
        $this->compare($class_name, get_class($class));

        unlink($file);
    }

    public function testAutoloadClass(){
        $class_name = $this->random();
        $class_code = $this->get_class_code($class_name);
        $file = $this->al_dir().'/'.$class_name.'.php';
        file_put_contents($file, $class_code);

        $lia = new \Lia();
        $package = new \Lia\Package($lia,'test1');
        $autoloader = new \Lia\Addon\Autoload($package);
        $autoloader->addDir($this->al_dir());
        $autoloader->enable();

        
        $class = new $class_name();
        $this->compare($class_name, get_class($class));

        unlink($file);

    }


    protected function al_dir(){
        return dirname(__DIR__,2).'/input/Al_Classes';
    }

    protected function get_class_code($class_name){
        $parts = explode('\\', $class_name);
        $class_name = array_pop($parts);
        $namespace = implode('\\', $parts);
        if ($namespace!=''){
            $namespace = 'namespace '.$namespace.';';
        }
        $code = 
            <<<PHP
            <?php
                {$namespace}
                class {$class_name} {}
            PHP;  
        return $code;
    }

    protected function random(){
        $bytes = random_bytes(15);
        $alpha = 'a'.bin2hex($bytes);
        return $alpha;
    }
}
