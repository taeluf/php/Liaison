<?php

namespace Liaison\Test\Addon;

class Router extends \Tlf\Tester {

    public function testPatternAndParamatersToUrl(){
        $router = new \Lia\Addon\Router();
        $pattern = '/blog/{category}/';
        $params = ['category'=>'some-category'];
        $target = '/blog/some-category/';

        $decoded = $router->decode_pattern($pattern);
        $filled = $router->decoded_pattern_to_url($decoded, $params);

        $this->compare(
            $target,
            $filled
        );
    }

    public function testExtractUrlParamaters(){
        $router = new \Lia\Addon\Router();
        $url = '/blog/some-category/';
        $pattern = '/blog/{category}/';
        $decoded = $router->decode_pattern($pattern);
        $extracted = $router->extract_url_paramaters($decoded, $url);

        $this->compare(
            ['category'=>'some-category'],
            $extracted
        );
    }

    public function testDecodePatternWithOptionalParamaters(){
        $router = new \Lia\Addon\Router();
        $pattern = '/blog/{?category}/';
        $decoded = $router->decode_pattern($pattern);

        $actual_decoded = $router->separate_optional_from_decoded_pattern($decoded);

        
        // $all = array_map(function($a){unset($a['extraParsedPattern']);unset($a['optionalParams']); return $a;},$all);
        // var_export($actual_decoded);
        // exit;

        $target = [
            [
                'pattern' => '/blog/{?category}/',
                'parsedPattern' => '/blog/?/',
                'params' => ['category'],
                'methods' => ['GET' => 'GET'],
            ],
            [
                'pattern' => '/blog/{?category}/',
                'parsedPattern' => '/blog/',
                'params' => [],
                'methods' => ['GET' => 'GET'],
            ],
        ];

        $this->compare($target, $actual_decoded);
    }

    /**
     * Example of `$router->url_to_regex()`
     */
    public function testUrlToRegex(){
        $router = new \Lia\Addon\Router();
        $url = '/one/two/';
        $test = $router->url_to_regex($url);

        $target = '^\/(?:one|\?)\/(?:two|\?)\/$';
        $this->compare($target ,$test);
    }

    /**
     * Example of `$router->decode_pattern()`
     */
    public function testDecodePattern(){
        $router = new \Lia\Addon\Router();

        $actual_parsed = $router->decode_pattern("/blog/{slug}/{id}/");

        $target_parsed = [
            "pattern" => "/blog/{slug}/{id}/",
            "parsedPattern" => "/blog/?/?/",
            "params" => [
                    0 => "slug",
                    1 => "id",
                ],
            "methods" => [
                "GET" => "GET",
            ],
        ];

        $this->compare($target_parsed, $actual_parsed);
    }

    /**
     * Example of `$router->decoded_pattern_to_url()`. @see(testParsePatternExample) to see what the `$decoded` array looks like
     *
     */
    public function testDecodedPatternToUrl(){
        $router = new \Lia\Addon\Router();
        $decoded = $router->decode_pattern("/blog/{slug}/{id}/");

        $fill = ['slug'=>'cats', 'id'=>33];
        $filled = $router->decoded_pattern_to_url($decoded, $fill);

        $this->compare('/blog/cats/33/', $filled);
    }

    public function testRemoveRoute(){
        $router = new \Lia\Addon\Router();
        $router->addRoute('/keep-one/', function(){});
        $router->addRoute('/keep-two/', function(){});
        $router->addRoute('/keep/{dynamic}/', function(){});
        $router->addRoute('/keep/static/', function(){});
        $router->addRoute('@POST./keep-post/', function(){});
        $router->addRoute('@GET./keep-get1/', function(){});
        $router->addRoute('@POST.@GET./keep-getpost/', function(){});

        $router->addRoute('/remove-one/', function(){});
        $router->addRoute('/remove-two/', function(){});
        $router->addRoute('/remove/{dynamic}/', function(){});
        $router->addRoute('/remove/static/', function(){});
        $router->addRoute('@POST./remove-post/', function(){});
        $router->addRoute('@GET./remove-get/', function(){});
        $router->addRoute('@POST.@GET./remove-getpost/', function(){});

        $router->removeRoute('/remove-one/');
        $router->removeRoute('/remove-two/');
        $router->removeRoute('/remove/{dynamic}/');
        $router->removeRoute('/remove/static/');
        $router->removeRoute('@POST./remove-post/');
        $router->removeRoute('@GET./remove-get/');
        $router->removeRoute('@POST.@GET./remove-getpost/');


        $get = array_keys($router->routeMap['GET']);
        $post = array_keys($router->routeMap['POST']);

        $get_target = [
            '/keep-one/',
            '/keep-two/',
            '/keep/?/',
            '/keep/static/',
            '/keep-get1/',
            '/keep-getpost/',
        ];

        $post_target = [
            '/keep-post/',
            '/keep-getpost/',
        ];


        $this->compare_arrays($get_target, $get);
        $this->compare_arrays($post_target, $post);

    }

    public function testRemoveReplaceRoute(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'adsfasddf');
        $router = new \Lia\Addon\Router($package);


        // @export_start(Router.RemoveReplaceRoute)
        $router = \Lia\Addon\Router::from($lia);

        // added by another package, perhaps 
        $router->addRoute('/bears/', __DIR__.'/bad-route-target.php');
        // remove the bad route
        $router->removeRoute('/bears/');
        // add your own route
        $router->addRoute('/bears/', __DIR__.'/my-better-version.php');
        // @export_end(Router.RemoveReplaceRoute)

        $this->compare(
            __DIR__.'/my-better-version.php',
            $router->routeMap['GET']['/bears/'][0]['target']
        );

        $this->is_false(isset($router->routeMap['GET']['/bears/'][1]));

    }
}
