<?php

namespace Liaison\Test\Addon;

class Error extends \Tlf\Tester {

    public function view_dir(){
        return $this->cli->pwd.'/test/input/Views/';
    }


    public function testErrorGoto(){
        $this->disable();
        return;
        $lia = new \Liaison();
        $routeContent = "Very generic content from a simple route.";
        \Lia\Addon\Router::from($lia)->addRoute('/',function($req,$resp)use($routeContent){$resp->content = $routeContent;});
        \Lia\Addon\Error::from($lia)->error_goto($errorMsg="Very generic error_goto message.");
        $response = \Lia\Addon\Server::from($lia)->getResponse('/', 'GET');

        //Check if redirect headers are correct
        //Then mock the state that would exist after the redirect
        //then getResponse('error_page_url')
        //and verify the error message is correct

        $this->str_contains($response->content, $errorMsg);
        $this->str_not_contains($response->content, $routeContent);
    }

    public function testErrorPage(){
        $_SERVER['REQUEST_URI'] = '/';
        $lia = new \Lia();
        $server = new \Lia\Package\Server($lia);
        $routeContent = "Very generic content from a simple route.";
        \Lia\Addon\Router::from($lia)->addRoute('/',function($req,$resp)use($routeContent){$resp->content = $routeContent;});
        \Lia\Addon\Error::from($lia)->error_page($errorMsg="Very generic error_page message.");
        $response = \Lia\Addon\Server::from($lia)->getResponse('/', 'GET');
        echo $response->content;
        $this->str_contains($response->content, $errorMsg);
        $this->str_not_contains($response->content, $routeContent);

    }

    public function testErrorHeader(){
        $_SERVER['REQUEST_URI'] = '/';
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test', $this->file('test/input/error-header-temp/'));
        $cache = new \Lia\Addon\Cache($package);
        $view = new \Lia\Addon\View($package);
        $server= new \Lia\Addon\Server($package);
        $router = new \Lia\Addon\Router($package);
        $hook = new \Lia\Addon\Hook($package);
        $resources = new \Lia\Addon\Resources($package);
        $error = new \Lia\Addon\Error($package);
        $package->init_lia();
        $package->ready();
        \Lia\Addon\View::from($lia)->addView('lia:theme', $this->view_dir());

        $routeContent = "Very generic content from a simple route.";
        \Lia\Addon\Router::from($lia)->addRoute('/',function($req,$resp)use($routeContent){$resp->content = $routeContent;});
        \Lia\Addon\Error::from($lia)->error_header($errorMsg="Very generic error_header message.");

        $response = $server->getResponse('/', 'GET');

        $this->str_contains($response->content, $errorMsg);
        $this->str_contains($response->content, $routeContent);
    }
    
    public function testSomeStatusCodes(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $router = new \Lia\Addon\Router($package);
        $error = new \Lia\Addon\Error($package);

        $codes = [
            100 => 'Continue',
            101 => 'Switching Protocol',
            102 => 'Processing',
            103 => 'Early Hints',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
        ];
        foreach ($codes as $num=>$text){
            $this->test("Code $num");
            $this->compare($text,$error->statusCodeText($num));
        }
    }
}
