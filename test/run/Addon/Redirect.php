<?php

namespace Liaison\Test\Addon;

class Redirect extends \Tlf\Tester {
    
    // public function testOld_Goto(){
    //     $this->disable();
    //     //this test fails due to stalling indefinitely
    //     // return;
    //     //
    //     $_SERVER['REQUEST_URI'] = '/request/from/cli/';
    //     $_SERVER['REQUEST_METHOD'] = 'GET';
    //     $lia = new \Liaison();
    //     \Lia\Addon\Router::from($lia)->addRoute($_SERVER['REQUEST_URI'],
    //         function($route){
    //             $lia=$route->package();
    //             $lia->goto('/black-lives-matter/');
    //         },
    //         $lia
    //     );
    //     $response = \Lia\Addon\Server::from($lia)->getResponse();
    //     var_export($response->headers);
    //     return $this->compare(
    //         [
    //           [
    //             'Location: /black-lives-matter/',
    //             302,
    //           ],
    //         ],
    //         $response->headers
    //     );
    // }

    public function testGoto(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test:goto');
        $goto = new \Lia\Addon\Redirect($package);
        new \Lia\Addon\Cache($package);
        $package->init_lia();
        \Lia\Addon\Cache::from($lia)->dir = $this->cache_dir();


        $message = 'abc';
        $target_url = '/test/';
        $goto->gotoWithMessage($target_url, $message);
        $actual_url = $goto->get_url();

        $parts = explode('message_id=', $actual_url);
        $_GET['message_id'] = $parts[1];

        
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test:goto');
        $receive_goto = new \Lia\Addon\Redirect($package);
        new \Lia\Addon\Cache($package);
        $package->init_lia();
        \Lia\Addon\Cache::from($lia)->dir = $this->cache_dir();

        $actual_message = $receive_goto->getGotoMessage();

        unset($_GET['message_id']);

        $this->compare($message, $actual_message);

        $this->empty_dir($this->cache_dir());

    }

    protected function cache_dir(){
        return $this->cli->pwd.'/test/input/Cache/redirect/';
    }
}


namespace Lia\Addon;

/** just being really lazy bc i don't want to test `header()` right now */
function header(){}
