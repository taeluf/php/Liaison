<?php

namespace Liaison\Test\Addon;

class Server extends \Tlf\Tester {

    public function testResolveConflictingRoutes(){

        $_SERVER['REQUEST_URI'] = 'na';
        $lia = new \Lia();
  
        // Add the built-in Package, which provides all the web-server features.    
        $server_package = new \Lia\Package\Server($lia, $fqn='lia:server');  // dir & base_url not required   
        
        \Lia\Addon\Server::from($lia)->useTheme = false;
        $router = \Lia\Addon\Router::from($lia);

        $dir = dirname(__DIR__,2).'/input/ConflictingRoutes/';
        // @export_start(Router.ResolveConflictingRoutes)
        // example routes. We want to choose the static routes over the dynamic routes for this example.
        $router->addRoute('/bear/sasha/', $dir.'/sasha-bear.php');
        $router->addRoute('/bear/jimbo/', $dir.'/jimbo-bear.php');
        $router->addRoute('/bear/{bearname}/', $dir.'/dynamic-bear.php');

        \Lia\Addon\Hook::from($lia)
            ->add(\Lia\Hooks::FILTER_ROUTE,
               function (\Lia\Obj\Route $route, array $routeList): bool { 
                   if (count($routeList)==1)return true; // there is only one route, allow it
                   else if ($route->paramaterizedPattern() == '/bear/{bearname}/'){
                       // this removes the dynamic route and allows the static routes
                        return false;
                   }
                   // always return true by default
                   return true;
               } 
        );
        // @export_end(Router.ResolveConflictingRoutes)

        
        $sasha_response = \Lia\Addon\Server::from($lia)->getResponse('/bear/sasha/');
        $jimbo_response = \Lia\Addon\Server::from($lia)->getResponse('/bear/jimbo/');
        $dynamic_response_tommy = \Lia\Addon\Server::from($lia)->getResponse('/bear/tommy/');
        $dynamic_response_strawbeary = \Lia\Addon\Server::from($lia)->getResponse('/bear/strawbeary/');

        $this->compare('static-sasha', $sasha_response->content);
        $this->compare('static-jimbo', $jimbo_response->content);
        $this->compare('dynamic-tommy', $dynamic_response_tommy->content);
        $this->compare('dynamic-strawbeary', $dynamic_response_strawbeary->content);
    }

    public function testSetTheme(){
        $_SERVER['REQUEST_URI'] = '/';
        $dir = $this->file('test/input/Themes/');

        $lia = new \Lia();
        $main = new \Lia\Package\Server($lia);

        $site = new \Lia\Package\Server($lia, 'lia:test', $dir);

        $response = \Lia\Addon\Server::from($lia)->getResponse();
        $this->str_contains($response->content, "--main theme--", "content", );

        \Lia\Addon\Server::from($lia)->setTheme('test:one');
        $response = \Lia\Addon\Server::from($lia)->getResponse();
        $this->str_contains($response->content, "--one--", "content", );


        \Lia\Addon\Server::from($lia)->setTheme('test:two');
        $response = \Lia\Addon\Server::from($lia)->getResponse();
        $this->str_contains($response->content, "--two--", "content", );

    }

    public function testResponseContentNoTheme(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test:idk');
        $server = new \Lia\Addon\Server($package);
        $server->init_lia();
        $server->useTheme = false;
        $hook = new \Lia\Addon\Hook($package);
        $hook->init_lia();
        $router = new \Lia\Addon\Router($package);
        $router->init_lia();
        $router->addRoute('/test/', [$this, 'sample_content']);

        $response = $server->getResponse('/test/');

        $this->compare('test-content', $response->content);
    }

    public function testResponseContentWithTheme(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test:idk');
        $server = new \Lia\Addon\Server($package);
        $server->init_lia();
        $view = new \Lia\Addon\View($package);
        $view->init_lia();
        $view->addViewCallable('theme', [$this, 'theme_view']);
        $hook = new \Lia\Addon\Hook($package);
        $hook->init_lia();
        $router = new \Lia\Addon\Router($package);
        $router->init_lia();
        $router->addRoute('/test/', [$this, 'sample_content']);

        $response = $server->getResponse('/test/');

        $this->compare('<theme>test-content</theme>', $response->content);
    }


    public function theme_view($name, $args){
        $response = $args['response'];
        return '<theme>'.$response->content.'</theme>';
    }

    public function sample_content($route, $response){
        $response->content = 'test-content';
    }
}
