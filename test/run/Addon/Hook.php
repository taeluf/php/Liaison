<?php

namespace Liaison\Test\Addon;


class Hook extends \Tlf\Tester {
    

    /**
     * Test hook features
     *
     * @tests adding and calling a hook 
     * @tests output of calling a hook (concatenation of output)
     * @tests return value of calling a hook (array of return values)
     */
    public function testHooks(){

        $hooks = new \Lia\Addon\Hook();
        $hooks->add('a.hook', function($input){echo "Input: $input"; return "V good";});
        $hooks->add('a.hook', function($input){echo "Uhhh: $input"; return "Vvvv good";});
        ob_start();
        $ret = $hooks->call('a.hook', "input, silly");
        $output = ob_get_clean();

        $this->compare(["V good", "Vvvv good"], $ret);
        $this->compare("Input: input, sillyUhhh: input, silly", $output);

    }


    protected function cache_dir(){
        return dirname(__DIR__,2).'/input/Cache';
    }
}

