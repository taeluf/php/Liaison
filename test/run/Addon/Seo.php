<?php

namespace Liaison\Test\Addon;

class Seo extends \Tlf\Tester {

    /**
     *
     * @tests seo() method
     */
    public function testSeoFromArray(){

        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test:idk');
        $seo = new \Lia\Addon\Seo($package);
        $seo->init_lia();

        //@export_start(Seo.FromArray)
        $seo = \Lia\Addon\Seo::from($lia);
        $seo_data = [
            'title' => 'Test Page',
            'description' => 'Test description',
            'image' => ['https://example.com/path/to/image.jpg', 'required alt text for image'],
            'url' => 'https://example.com/canonical/url/',
            'site_name' => 'Liaison test',
            'keywords'=> 'blog,politics,fascism,2025',
        ];
        $seo->seo($seo_data);
        //@export_end(Seo.FromArray)
        $html = $seo->get_html();

        $tHtml =
        <<<HTML
        <title>Test Page</title>
        <meta property="og:title" content="Test Page" />
        <meta name="description" content="Test description" />
        <meta property="og:description" content="Test description" />
        <meta property="og:type" content="article" />
        <meta property="twitter:card" content="summary" />
        <meta property="og:image" content="https://example.com/path/to/image.jpg" />
        <meta property="twitter:image" content="https://example.com/path/to/image.jpg" />
        <meta property="og:image:alt" content="required alt text for image" />
        <link rel="canonical" href="https://example.com/canonical/url/" />
        <meta name="og:url" content="https://example.com/canonical/url/" />
        <meta name="og:site_name" content="Liaison test" />
        <meta name="keywords" content="blog,politics,fascism,2025" />
        HTML;

        $tHtml = explode("\n",$tHtml);
        $tHtml = array_map('trim', $tHtml);
        $tHtml = trim(implode("\n",$tHtml));
 
        $this->compare_lines($tHtml, $html);
    }

    /**
     *
     * @tests liaison seo methods
     * @tests seo html output
     * @note if the underlying html changes, just copy+paste output from the test & copy it into the HTML HEREDOC
     */
    public function testSeoLiaMethods(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test:idk');
        $seo = new \Lia\Addon\Seo($package);
        $seo->init_lia();

        //@export_start(Seo.MainMethods)
        $seo = \Lia\Addon\Seo::from($lia);
        $seo->title('Test Page'); // <title> and og:title
        $seo->description('Test description'); // <meta name="description">, og:title, twitter:card summary, and og:type article
        $seo->image('https://example.com/path/to/image.jpg', 'required alt text for image'); // og:image, twitter:image, og:image:alt
        $seo->url('https://example.com/canonical/url/'); // link canonical, og:url
        $seo->site_name('Liaison test'); // og:site_name
        $seo->keywords('blog,politics,fascism'); // meta name="keywords"
        //@export_end(Seo.MainMethods)

        $html = $seo->get_html();

        $aHtml = trim($html);
        $tHtml =
        <<<HTML
        //@export_start(Seo.MainMethodsOutput)
        <title>Test Page</title>
        <meta property="og:title" content="Test Page" />
        <meta name="description" content="Test description" />
        <meta property="og:description" content="Test description" />
        <meta property="og:type" content="article" />
        <meta property="twitter:card" content="summary" />
        <meta property="og:image" content="https://example.com/path/to/image.jpg" />
        <meta property="twitter:image" content="https://example.com/path/to/image.jpg" />
        <meta property="og:image:alt" content="required alt text for image" />
        <link rel="canonical" href="https://example.com/canonical/url/" />
        <meta name="og:url" content="https://example.com/canonical/url/" />
        <meta name="og:site_name" content="Liaison test" />
        <meta name="keywords" content="blog,politics,fascism" />
        //@export_end(Seo.MainMethodsOutput)
        HTML;

        $tHtml = explode("\n",$tHtml);
        $tHtml = array_map('trim', $tHtml);
        $tHtml = trim(implode("\n",$tHtml));
        $aHtml = 
            '//@export_start'.'(Seo.MainMethodsOutput)'
                ."\n".$html
            ."\n".'//@export_end'.'(Seo.MainMethodsOutput)';

        $this->compare_lines($tHtml, $aHtml);
        //$this->compare($tHtml, $aHtml);

    }

}
