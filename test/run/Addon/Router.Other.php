<?php

namespace Liaison\Test\Addon;

/**
 * Old router tests that are sloppy, confusing, hard to understand but still provide utility.
 */
class RouterOther extends \Tlf\Tester {

    //@TODO Fill in placeholder tests for Router

    // public function testNotImplemented_InvalidMethods(){
    //     $this->disable();
    //     echo "This is not implemented currently, but the test-method is left here as a placeholder, as a note.";
    // }
    // public function testPublicDirRouting(){
    //     $this->disable();
    // }
    // public function testConflictBetweenRoutesWithDifferentVarCounts(){
    //     $this->disable();
    // }
    // public function testConflictBetweenRoutesWithSameVarCounts(){
    //     $this->disable();
    // }
    // public function testConflictBetweenStaticRouteAndVarRoute(){
    //     $this->disable();
    // }
    //
    // public function testCustomRouteMethod(){
    //     $this->disable();
    //     // echo "This is not implemented currently, but the test-method is left here as a placeholder, as a note.";
    //     return false;
    // }

    /**
     *
     * @note this test changes the varDelim
     */
    public function testOptionalParamater(){
        // this test has been failing for awhile & I'm not fixing it right now.
        $lia = new \Lia();
        $package = new \Lia\Package($lia,'asdfasd');
        $router = new \Lia\Addon\Router($package);
        $router->varDelim = '\\.\\/\\:';
        
        \Lia\Addon\Router::from($lia)->addRoute('/optional/{?checkingFor}/and/{required}/',
            function($route){
//                 print_r($route->paramaters());
                return $route->param('checkingFor').'::'.$route->param('required');
            }
        );
        
        
        $route1 = $router->route(new \Lia\Obj\Request('/optional/abc/and/the-required/'))[0];
        $callable1 = $route1->target();
        $ret1 = $callable1($route1);
        
        $this->compare('abc::the-required',$ret1);
        
        $route2 = $router->route(new \Lia\Obj\Request('/optional/and/the-required/'))[0];
        $callable2 = $route2->target();
        $ret2 = $callable2($route2);
        
        $this->compare('::the-required',$ret2);
        
    }

    public function testGetRoute(){
        // this test has been failing for awhile & I'm not fixing it right now.
        // $this->setup($lia, $router, $package);
        $lia = new \Lia();
        $package = new \Lia\Package($lia,'asdfasd');
        $router = new \Lia\Addon\Router($package);

        $c = function($route){};
        \Lia\Addon\Router::from($lia)->addRoute(
            $tParamPattern='@GET.@POST./ban/toxic/{type}/'
        ,$c);
        $tPlaceholderPattern = '/ban/toxic/?/';
        
        $aRouteList = $router->route(new \Lia\Obj\Request($tUrl='/ban/toxic/chemicals/',$tMethod='POST'));
        $tReg = $router->url_to_regex($tUrl);

        $active = [
            'url' => $tUrl,
            'method'=>$tMethod,
            'urlRegex'=>$tReg,
        ];
        $shared = [
            'paramaters'=>['type'=>'chemicals']
        ];
        $static = [
            'allowedMethods'=>["GET"=>"GET", "POST"=>"POST"],
            'paramaterizedPattern'=>$tParamPattern,
            'placeholderPattern'=>$tPlaceholderPattern,
            'target'=>$c,
            'package'=>null,
        ];
        $route = new \Lia\Obj\Route(array_merge($active,$shared,$static));
        $tRouteList = [
            $route
        ];

        $this->compare_raw($tRouteList, $aRouteList);
    }

    public function testUrlToTestReg(){
        // $this->setup($lia, $router, $package);
        $router = new \Lia\Addon\Router();
        $urls = [
            '/one/two/three/'=>'^\\/(?:one|\?)\\/(?:two|\?)\\/(?:three|\?)\\/$',
            '/one.two.three/'=>'^\\/(?:one|\?)\\.(?:two|\?)\\.(?:three|\?)\\/$',
            '/one.two-three/four'=>'^\\/(?:one|\?)\\.(?:two|\?)\\-(?:three|\?)\\/(?:four|\?)$',
            '/five/six.seven.eight/.nine'=>'^\\/(?:five|\?)\\/(?:six|\?)\\.(?:seven|\?)\\.(?:eight|\?)\\/\\.(?:nine|\?)$',
            '/-thing-/'=>'^\\/\\-(?:thing|\?)\\-\\/$',
            '/.thing./'=>'^\\/\\.(?:thing|\?)\\.\\/$',
            '/..../'=>'^\\/\\.\\.\\.\\.\\/$'
        ];
        $success = true;
        foreach ($urls as $url=>$target){
            $actual = $router->url_to_regex($url);
            $this->test($url)
                 ->compare($target, $actual);
        }
    }


    public function testParsePatterns(){
        // $this->setup($lia, $router, $package);
        $router = new \Lia\Addon\Router();
        $routes = [
            'static'=>'/black-lives-matter/',
            '/save/the/environment/',
            'params'=>['/{any}.{person}/deserves/respect/','/?.?/deserves/respect/',['any','person']],
            ['/abc/{dyn}/something/', '/abc/?/something/', ['dyn']],
            ['/abc/def/{dynam}.{two}/', '/abc/def/?.?/', ['dynam','two']],
            ['/abc/def/{dynam}-{two}', '/abc/def/?-?', ['dynam','two']],
            ['/abc/def/{dynam}:{two}', '/abc/def/?:?', ['dynam','two']],
            ['/abc/def/{dynam}{two}.{abc}', '/abc/def/{dynam}{two}.?',['abc']],
            'method'=>['/@POST.greed/kills/@GET.people/','/greed/kills/people/',null,['GET','POST']],
            ['/flarg/harp/@POST.@GET.{dyn}/', '/flarg/harp/?/',['dyn'], ['GET','POST']],
            ['@POST./regulations/on/{megacorp}/', '/regulations/on/?/',['megacorp'],['POST']]
        ];
        $success = true;
        foreach ($routes as $key => $r){
            if (is_string($r))$r = [$r];
            $t = [];
            $t['pattern'] = $r[0];
            $t['parsedPattern'] = $r[1] ?? $r[0];
            $t['params'] = $r[2] ?? [];
            $tMethods = $r[3] ?? ['GET'];
            $t['methods'] = array_combine($tMethods,$tMethods);
            ksort($t['methods']);
            $actual = $router->decode_pattern($r[0]);
            ksort($actual['methods']);
            krsort($actual);
            krsort($t);
            if (is_string($key))$this->test($key);
            $this->compare($t, $actual,true);
        }

    }

    // public function testBenchmarktestPatternRouteMethod(){
        // $this->disable();
        // echo 'to save energy, this benchmark is disabled, as it does not actually test anything.';
        // return false;
        // $i=0;
        // ob_start();
        // while ($i++<100)$this->testPatternRouteMethod();
        // ob_get_clean();
        // return true;
    // }
    // public function testBenchmarkGetRoute(){
        // $this->disable();
        // echo 'to save energy, this benchmark is disabled, as it does not actually test anything.';
        // return false;
        // $i=0;
        // ob_start();
        // while ($i++<100)$this->testGetRoute();
        // ob_get_clean();
        // return true;
    // }
    // public function testBenchmarkUrlToTestReg(){
        // $this->disable();
        // echo 'to save energy, this benchmark is disabled, as it does not actually test anything.';
        // return false;
        // $i=0;
        // ob_start();
        // while ($i++<100)$this->testUrlToTestReg();
        // ob_get_clean();
        // return true;
    // }
    // public function testBenchmarkParsePatternsOneHundredTimes(){
        // $this->disable();
        // echo 'to save energy, this benchmark is disabled, as it does not actually test anything.';
        // return false;
        // $i=0;
        // ob_start();
        // while ($i++<100)$this->testParsePatterns();
        // ob_get_clean();
        // return true;
    // }

}
