<?php

namespace Lia\Test\Addon;

class ResourceSorter extends \Tlf\Tester {

    public function testSortJs(){


        $resources_to_add = [
            ["Bind.js"],
            ["autowire.js"],
            ["SimpleRequest.js"],
            ["autowire.js"],
            [
                "TagGroup/Search.js",
                "TagGroup/Group.js",
                "TagGroup/GroupItem.js",
                "TagGroup/TagButton.js",
                "TagGroup/Mode.js",
            ],
            [ "autowire.js", ],
            [
                "TagGroup/Search.js",
                "TagGroup/Group.js",
                "TagGroup/GroupItem.js",
                "TagGroup/TagButton.js",
                "TagGroup/Mode.js",
            ],
        ];

        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test:sorter');
        $res = new \Lia\Addon\Resources($package);

        $sorter = new class($package) extends \Lia\Addon\ResourceSorter{
            public function get_list(): array { return $this->orders; }
        };
        $res->init_lia();
        $sorter->init_lia();

        foreach ($resources_to_add as $resource_list){
            $sorter->setResourceOrder('js', $resource_list, false);
        }

        $target = [
            'Bind.js', 'autowire.js', 'SimpleRequest.js',
            "TagGroup/Search.js",
            "TagGroup/Group.js",
            "TagGroup/GroupItem.js",
            "TagGroup/TagButton.js",
            "TagGroup/Mode.js",
       ];
        $target = array_combine($target, $target);

        //class 

        $this->compare_arrays($target, $sorter->get_list()['js']);
        //exit;
    }

    public function testSortCss(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test:sorter');
        $res = new \Lia\Addon\Resources($package);
        $sorter = new \Lia\Addon\ResourceSorter($package);
        $res->init_lia();
        $sorter->init_lia();

        $input = [
            'three.css',
            'five.css',
            'two.css',
            'four.css',
            'one.css',
            'six.css',
        ];
        $input = array_map(function($file){return $this->dir().'/'.$file;}, $input);
        //$dir = $this->dir();
        //foreach ($input as $if){
            //\Lia\Addon\Resources::from($lia)->addFile($dir.$if);
        //}

        // @export_start(ResourceSorter.RelativeCssSort)
        \Lia\Addon\ResourceSorter::from($lia)
            ->setResourceOrder(
                $extension='css', // js for javascript files
                $relative_file_paths= // an array of relative file paths, in the order you want them. There MUST NOT be leading `/`. There MAY be as many `/` as you like
                    ['one.css', 
                    'two.css', 
                    'three.css', 
                    'four.css',
                    ],
                $prepend=false // FALSE will put these names, in this order, at the end of the list of all css files. TRUE will put them at the beginning in this order. 
            );
        // @export_end(ResourceSorter.RelativeCssSort)

        $resources = $sorter->getSortedFiles($input);
        // remove dirname
        $resources = array_map(function($f){return basename(dirname($f)).'/'.basename($f);}
                                ,$resources);
        $target = [
            'ResourceSorter/one.css',
            'ResourceSorter/two.css',
            'ResourceSorter/three.css',
            'ResourceSorter/four.css',
            'ResourceSorter/five.css',
            'ResourceSorter/six.css',
        ];
        
        $this->test('Sorts files correctly');
        $this->compare_arrays($target, $resources);
        $this->test('Has unsorted files, too');
        $this->compare(6, count($resources));
        
    }

    public function dir(){
        return $this->cli->pwd.'/test/input/ResourceSorter/';
    }

}
