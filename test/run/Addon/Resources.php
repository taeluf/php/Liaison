<?php

namespace Lia\Test\Addon;

class Resources extends \Tlf\Tester {
    //
    // public function testSomething(){
    //     $this->disable();
    //     //add script file
    //     //add script file
    //     //add script file
    //     //sort script files
    //     //save them
    //     //get output html
    //     //deliver the cached javascript file
    //     // verify output matches sorted order
    //     //maybe test cache length?
    //     // definitely test explicit cache invalidation
    // }
    //
    // public function testIsUsingCacheInsteadOfRecompiling(){
    //
    //     $this->disable();
    //     return false;
    // }
    // public function testSeparateLargeFile(){
    //     $this->disable();
    // }
    //
    // public function testAddCode(){
    //     echo 'test not written yet';
    //     $this->disable();
    // }
    // public function testGetHtml(){
    //     //Check for seo html, compiled resource html, url html, and explicit code blocks
    //     echo "Between testSeoFunctions and testCompiledHtml, this is basically being done.";
    //     $this->disable();
    // }
    // public function testSeoImageRequiresAlt(){
    //     $this->disable();
    // }
    // public function testSeoArray(){
    //     $this->disable();
    // }

    /** for documentation generation purposes */
    public function testAddCssAndJavascriptFiles(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        new \Lia\Addon\Resources($package);

        // @export_start(ResourcesAddon.AddResources)
        $resources = \Lia\Addon\Resources::from($lia); // your Liaison instance

        // Files will be concatenated and added to a singular css file
        $private_dir = dirname(__DIR__,2).'/input/private/resources/';
        $resources->addFile($private_dir.'/undeliverable-styles.css');
        $resources->addFile($private_dir.'/undeliverable-scripts.js');

        // URLs will be added as `<link>` and `<script>` tags and added to the HTML head
        $resources->addURL('/resources/page-layout.css'); // deliverable on your site
        $resources->addURL('https://cdn.example.com/my-namespace/dancing-bear.js'); // external file
        // @export_end(ResourcesAddon.AddResources)

        $scriptTags = $resources->getUrlTag('js');
        $cssTags = $resources->getUrlTag('css');
        $target_tags = 
<<<HTML
    <script type="text/javascript" src="https://cdn.example.com/my-namespace/dancing-bear.js"></script>
    <link rel="stylesheet" href="/resources/page-layout.css" />
HTML;

        $this->compare_lines($target_tags, trim($scriptTags)."\n".trim($cssTags));


        $js_code = $resources->concatenateFiles('js');
        $css_code = $resources->concatenateFiles('css');

        $target_js = file_get_contents($private_dir.'/undeliverable-scripts.js');
        $target_css = file_get_contents($private_dir.'/undeliverable-styles.css');

        $this->compare_lines($target_js, $js_code);
        $this->compare_lines($target_css, $css_code);




    }

    public function testAddUrl(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();

        for ($i=0;$i<15;$i++){
            $ext = $i%2 ? 'css' : 'js';
            \Lia\Addon\Resources::from($lia)->addURL('https://test.taeluf.com/'.$i.'.'.$ext);
        }
        
        $scriptTags = $res->getUrlTag('js');
        $cssTags = $res->getUrlTag('css');
        
        $tScripts = 
<<<HTML
    <script type="text/javascript" src="https://test.taeluf.com/0.js"></script>
    <script type="text/javascript" src="https://test.taeluf.com/2.js"></script>
    <script type="text/javascript" src="https://test.taeluf.com/4.js"></script>
    <script type="text/javascript" src="https://test.taeluf.com/6.js"></script>
    <script type="text/javascript" src="https://test.taeluf.com/8.js"></script>
    <script type="text/javascript" src="https://test.taeluf.com/10.js"></script>
    <script type="text/javascript" src="https://test.taeluf.com/12.js"></script>
    <script type="text/javascript" src="https://test.taeluf.com/14.js"></script>
HTML;    

        $tStyles = 
<<<HTML
    <link rel="stylesheet" href="https://test.taeluf.com/1.css" />
    <link rel="stylesheet" href="https://test.taeluf.com/3.css" />
    <link rel="stylesheet" href="https://test.taeluf.com/5.css" />
    <link rel="stylesheet" href="https://test.taeluf.com/7.css" />
    <link rel="stylesheet" href="https://test.taeluf.com/9.css" />
    <link rel="stylesheet" href="https://test.taeluf.com/11.css" />
    <link rel="stylesheet" href="https://test.taeluf.com/13.css" />
HTML;    

        $this->compare($tScripts, $scriptTags);
        $this->compare($tStyles, $cssTags);
    }

    public function testCompiledHtml(){
        $_SERVER['REQUEST_URI'] = '/';
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $package->dir = $this->dir().'/cache/';
        $res = new \Lia\Addon\Resources($package);
        $res->onPackageReady();
        $cache = new \Lia\Addon\Cache($package);
        $res->init_lia();
        $cache->init_lia();




        $cache->dir = $this->dir().'/cache/';
        $res->useCache = false;

        $filesDir = $this->dir().'/file/';
        $content = [];
        foreach (scandir($filesDir) as $file){
            $ext = pathinfo($file,PATHINFO_EXTENSION);
            if ($ext!='js'&&$ext!='css')continue;
            \Lia\Addon\Resources::from($lia)->addFile($filesDir.'/'.$file);
        }
        $jsFile = $res->compileFilesToCache('js');
        $cssFile = $res->compileFilesToCache('css');
        
        $scriptTag = $res->getFileTag('js');
        $cssTag = $res->getFileTag('css');

        $html = \Lia\Addon\Resources::from($lia)->getHtml();

        $aHtml = explode("\n",$html);
        $aHtml = array_map('trim',$aHtml);
        $aHtml = array_filter($aHtml);
        sort($aHtml);
        $tHtml = [$scriptTag, $cssTag];
        sort($tHtml);

        $this->compare($tHtml, $aHtml);

        $this->empty_dir($cache->dir);
    }

    public function testDuplicateCompile(){
        // this test has been failing for awhile & I'm not fixing it right now.
        $this->disable();
        return;
        $lia = new \Liaison(['bare'=>true]);
        $package = new \Liaison\Test\Mock\Package($lia);
        $cache = new \Lia\Compo\Cache($package);
        $lia->set('lia:resource.forceRecompile', true);
        $lia->set('lia:cache.dir', $this->cacheDir);
        $res = new \Lia\Compo\Resources($package);

        $filesDir = $this->outDir;
        $content = [];
        foreach (scandir($filesDir) as $file){
            $ext = pathinfo($file,PATHINFO_EXTENSION);
            if ($ext!='js'&&$ext!='css')continue;
            \Lia\Addon\Resources::from($lia)->addFile($filesDir.'/'.$file);
        }

        $jsFile = $res->compileFilesToCache('js');
        $jsFile1 = $res->compileFilesToCache('js');
        $jsFile2 = $res->compileFilesToCache('js');
        $jsFile3 = $res->compileFilesToCache('js');

        $cssFile = $res->compileFilesToCache('css');
        $cssFile1 = $res->compileFilesToCache('css');
        $cssFile2 = $res->compileFilesToCache('css');
        $cssFile3 = $res->compileFilesToCache('css');
        
        return true
        &&  $this->compare($jsFile,$jsFile1)
        &&  $this->compare($jsFile1,$jsFile2)
        &&  $this->compare($jsFile2, $jsFile3)

        &&  $this->compare($cssFile,  $cssFile1)
        &&  $this->compare($cssFile1, $cssFile2)
        &&  $this->compare($cssFile2, $cssFile3)

        ;
    }

    public function testDeliverCss(){
        // this test has been failing for awhile & I'm not fixing it right now.
        $this->disable();
        return;
        $lia = new \Liaison();
        $res = $lia->compo('lia:Resources');
        $package = $lia->getPackage('lia');

        \Lia\Addon\View::from($lia)->addViewCallable('theme',
            function($name, $args){
                return $args['content'];
            }
        );
        $lia->set('lia:cache.dir', $this->cacheDir);
        $lia->set('lia:resource.forceRecompile', true);
        $lia->set('lia.Server.bufferResponse', false);
        
        $filesDir = $this->outDir;
        $content = [];
        foreach (scandir($filesDir) as $file){
            if (pathinfo($file,PATHINFO_EXTENSION)!='css')continue;
            \Lia\Addon\Resources::from($lia)->addFile($filesDir.'/'.$file);
            $content[] = file_get_contents($filesDir.'/'.$file);
        }

        $aRoute = $res->getCompiledFilesUrl('css');

        ob_start();
        \Lia\Addon\Server::from($lia)->deliver($aRoute, "GET");
        $aContent = ob_get_clean();

        $tStyles = implode("\n", $content);

        return true
        &&  $this->compare(strlen($tStyles)>5, true, true)
        &&  $this->compare($tStyles, $aContent);
        
        ;
    }

    public function testDeliverJs(){
        // this test has been failing for awhile & I'm not fixing it right now.
        $this->disable();
        return;
        $lia = new \Liaison();
        
        $res = $lia->compo('Resources');
        $package = $lia->getPackage('lia');

        \Lia\Addon\View::from($lia)->addViewCallable('theme',
            function($name, $args){
                return $args['content'];
            }
        );
        $lia->set('lia:cache.dir', $this->cacheDir);
        $lia->set('lia:resource.forceRecompile', true);
        $lia->set('lia.Server.bufferResponse', false);


        $filesDir = $this->outDir;
        $content = [];
        foreach (scandir($filesDir) as $file){
            if (pathinfo($file,PATHINFO_EXTENSION)!='js')continue;
            \Lia\Addon\Resources::from($lia)->addFile($filesDir.'/'.$file);
            $content[] = file_get_contents($filesDir.'/'.$file);
        }

        $aRoute = $res->getCompiledFilesUrl('js');

        ob_start();
        try {
            \Lia\Addon\Server::from($lia)->deliver($aRoute, "GET");
        } catch (\Exception $e){
            $aContent = ob_get_clean();
            throw $e;
        }
        $aContent = ob_get_clean();

        $tScripts = implode("\n", $content);


        $this->compare(strlen($tScripts)>5, true, true);
        $this->compare($tScripts, $aContent);
    }
    
    public function testCacheCompiledJSFile(){
        $_SERVER['REQUEST_URI'] = '/';
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $package->dir = $this->dir().'/cache/';
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();
        $res->onPackageReady();

        $cache = new \Lia\Addon\Cache($package);
        $cache->init_lia();


        // $lia->set('cache.dir', $this->dir().'/cache/');

        $cache->dir = $this->dir().'/cache/';
        $filesDir = $this->dir().'/file/';

        $content = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-3)!='.js')continue;
            \Lia\Addon\Resources::from($lia)->addFile($filesDir.'/'.$file);
            $content[] = file_get_contents($filesDir.'/'.$file);
        }

        $tFile = $res->compileFilesToCache('js');
        $tScripts = implode("\n", $content);
        $this->compare(strlen($tScripts)>5, true, true);
        $this->compare($tScripts, file_get_contents($tFile));

        $this->empty_dir($this->dir().'/cache/');
    }

    public function testCacheCompiledCSSFile(){
        $_SERVER['REQUEST_URI'] = '/';
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $package->dir = $this->dir().'/cache/';
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();
        $res->onPackageReady();

        $cache = new \Lia\Addon\Cache($package);
        $cache->init_lia();

        // $lia->set('cache.dir', $this->dir().'/cache/');

        $filesDir = $this->dir().'/file/';
        $content = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-4)!='.css')continue;
            \Lia\Addon\Resources::from($lia)->addFile($filesDir.'/'.$file);
            $content[] = file_get_contents($filesDir.'/'.$file);
        }

        $tFile = $res->compileFilesToCache('css');
        $tStyles = implode("\n", $content);

        $this->compare(strlen($tStyles)>5, true, true);
        $this->compare($tStyles, file_get_contents($tFile));

        $this->empty_dir($this->dir().'/cache/');
    }

    public function testSortCss(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();

        $filesDir = $this->dir().'/file/';
        $content = [];
        $files = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-4)!='.css')continue;
            $files[$file] = $filesDir.'/'.$file;
            \Lia\Addon\Resources::from($lia)->addFile($filesDir.'/'.$file);
        }
        $files = [
            'b.css' => $files['b.css'],
            'a.css' => $files['a.css'],
            'c.css' => $files['c.css'],
        ];
        foreach ($files as $f){
            $content[] = file_get_contents($f);
        }

        // @export_start(ResourcesAddon.FunctionSorter)
        \Lia\Addon\Resources::from($lia)->setSorter('css',
            /** $files is an array of absolute paths. */
            function(array $files): array{
                $files = array_values($files);
                return
                [
                    $files[1], // b.css
                    $files[0], //a.css
                    $files[2], //c.css
                ];
            }
        );
        // @export_end(ResourcesAddon.FunctionSorter)
        
        $aStyles = $res->concatenateFiles('css');
        $tStyles = implode("\n", $content);

        $this->compare(strlen($tStyles)>5, true, true);
        $this->compare($tStyles, $aStyles);
    }
    /** 
     * Test that the Resource Sorter is able to REMOVE a css file from the list.
     */
    public function testRemoveCssFile(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();

        $filesDir = $this->dir().'/file/';
        $content = [];
        $files = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-4)!='.css')continue;
            $files[$file] = $filesDir.'/'.$file;
            \Lia\Addon\Resources::from($lia)->addFile($filesDir.'/'.$file);
        }
        $files = [
            'b.css' => $files['b.css'],
            //'a.css' => $files['a.css'],
            'c.css' => $files['c.css'],
        ];
        foreach ($files as $f){
            $content[] = file_get_contents($f);
        }

        // @export_start(ResourcesAddon.FunctionRemover)
        \Lia\Addon\Resources::from($lia)->setSorter('css',
            /** $files is an array of absolute paths. */
            function(array $files): array{
                $files = array_values($files);
                return
                [
                    // $files[0], //a.css is removed from the list.
                    $files[1], // b.css
                    $files[2], //c.css
                ];
            }
        );
        // @export_end(ResourcesAddon.FunctionRemover)
        
        $aStyles = $res->concatenateFiles('css');
        $tStyles = implode("\n", $content);

        $this->compare(strlen($tStyles)>5, true, true);
        $this->compare($tStyles, $aStyles);
    }

    public function testCompileCss(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();


        $filesDir = $this->dir().'/file/';
        $content = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-4)!='.css')continue;
            \Lia\Addon\Resources::from($lia)->addFile($filesDir.'/'.$file);
            $content[] = file_get_contents($filesDir.'/'.$file);
        }
        $aStyles = $res->concatenateFiles('css');
        $tStyles = implode("\n", $content);

        $this->compare(strlen($tStyles)>5, true, true);
        $this->compare($tStyles, $aStyles);
    }


    public function testSortJs(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();
        $filesDir = $this->dir().'/file/';
        $content = [];
        $files = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-3)!='.js')continue;
            $files[$file] = $filesDir.'/'.$file;
            \Lia\Addon\Resources::from($lia)->addFile($filesDir.'/'.$file);
        }
        $files = [
            'b.js' => $files['b.js'],
            'a.js' => $files['a.js'],
            'c.js' => $files['c.js'],
        ];
        foreach ($files as $f){
            $content[] = file_get_contents($f);
        }

        $res->setSorter('js',
            function($files){
                $files = array_values($files);
                return
                [
                    'b.js' => $files[1],
                    'a.js' => $files[0],
                    'c.js' => $files[2],
                ];
            }
        );
        
        $aScripts = $res->concatenateFiles('js');
        $tScripts = implode("\n", $content);
        $this->compare(strlen($tScripts)>5, true, true);
        $this->compare($tScripts, $aScripts);
    }

    public function testCompileJs(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();
        $filesDir = $this->dir().'/file/';
        $content = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-3)!='.js')continue;
            \Lia\Addon\Resources::from($lia)->addFile($filesDir.'/'.$file);
            $content[] = file_get_contents($filesDir.'/'.$file);
        }
        $aScripts = $res->concatenateFiles('js');
        $tScripts = implode("\n", $content);

        $this->compare(strlen($tScripts)>5, true, true);
        $this->compare($tScripts, $aScripts);
    }

    protected function dir(){
        return $this->cli->pwd . '/test/input/Resources/';
    }
}

