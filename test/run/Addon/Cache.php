<?php

namespace Liaison\Test\Addon;


class Cache extends \Tlf\Tester {
    

    public function testDeleteStaleFiles(){
        $cache = new \Lia\Addon\Cache();
        $cache->dir = $this->cache_dir().'/stale-check/';

        $new_file_expiry = -1 * ( ($cache->stale_check_frequency * 60) + 3600 );
        $file_names = ['.', '..'];
        $kept_files = ['.', '..'];

        /////
        // pre-clear the dir
        /////
        foreach (scandir($cache->dir) as $f){
            if ($f=='.' || $f == '..')continue;
            unlink($cache->dir.'/'.$f);
        }


        /////
        // add a couple files that will NOT be deleted
        /////
        $cache->cache_file($name=uniqid(), 'happy', time()+3600);
        $file_names[] = basename($cache->cache_file_path($name));
        $file_names[] = basename($cache->cache_meta_file_path($name));
        $kept_files[] = basename($cache->cache_file_path($name));
        $kept_files[] = basename($cache->cache_meta_file_path($name));
        $cache->cache_file($name=uniqid(), 'happy', time()+60);
        $file_names[] = basename($cache->cache_file_path($name));
        $file_names[] = basename($cache->cache_meta_file_path($name));
        $kept_files[] = basename($cache->cache_file_path($name));
        $kept_files[] = basename($cache->cache_meta_file_path($name));


        /////
        // create cache files
        /////
        $i=0;
        while ($i++ < 20){
            $name = uniqid();
            $file_names[] = basename($cache->cache_file_path($name));
            $file_names[] = basename($cache->cache_meta_file_path($name));
            $cache->cache_file($name, 'happy', $new_file_expiry);//it expired 5 seconds ago LOL
        }
        // add a couple fies that should NOT be deleted
        
        /////
        // verify cache files were created
        /////
        $this->test('ensure new cache files are present');
        $real_files = scandir($cache->dir);
        sort($real_files);
        sort($file_names);
        $this->compare($file_names, $real_files);

        /////
        // actually run the thing
        /////
        $cache->run_delete_stale_files();

        /////
        // verify cache files were deleted & that non-stale ones were kept
        /////
        $this->test('check that stale files were deleted & non-stale were kept');
        $real_files = scandir($cache->dir);
        sort($real_files);
        sort($kept_files);

        $this->compare($kept_files, $real_files);
        $this->compare(6, count($kept_files));

        /////
        // post-clear the dir
        /////
        foreach (scandir($cache->dir) as $f){
            if ($f=='.' || $f == '..')continue;
            unlink($cache->dir.'/'.$f);
        }
    }


    public function testFileCache(){

        $cache = new \Lia\Addon\Cache();
        // $cache->lia->set('cache.dir',$dir=$this->cache_dir());
        $cache->dir = $dir=$this->cache_dir();
        $cache->cache_file($file_name='file-name', $file_content='i rarely know enough to form a real opinion.');

        $file_path = $cache->cache_file_path($file_name);
        $meta_path = $cache->cache_meta_file_path($file_name);

        $this->test('cached file stored successfully & retrieves as expected');
        $this->compare(true, $cache->is_cache_file_valid($file_name));
        $this->compare($file_path, $cache->get_cache_file_path($file_name));
        $this->compare($file_content, 
            file_get_contents($cache->cache_file_path($file_name)));
        $this->compare($file_content, 
            file_get_contents($cache->get_cache_file_path($file_name)));
        $this->compare($file_content, 
            $cache->get_cache_file_content($file_name));


        $this->test('cached file unavailable if cache disabled');
        $cache->enabled = false;
        $this->compare(false, $cache->get_cache_file_path($file_name));
        $this->compare(false, $cache->is_cache_file_valid($file_name));
        $this->compare(true, file_exists($cache->cache_file_path($file_name)));

        $this->test('cached file unavailable if expired');
        $old_cache = file_get_contents($meta_path);
        $new_cache = json_encode(['expiry'=>time()-100]);
        file_put_contents($meta_path, $new_cache);
        $this->compare(false, $cache->get_cache_file_path($file_name));
        $this->compare(false, $cache->is_cache_file_valid($file_name));
        $this->compare(true, file_exists($cache->cache_file_path($file_name)));


        $this->test('cached file unavailable if file deleted');
        file_put_contents($meta_path, $new_cache);
        $cache->enabled = true;

        unlink($file_path);
        $this->compare(false, $cache->get_cache_file_path($file_name));
        $this->compare(false, $cache->is_cache_file_valid($file_name));
        $this->compare(false, file_exists($cache->cache_file_path($file_name)));

        unlink($meta_path);
    }

    /**
     * Tests \Lia\Addon\Cache::from($lia)->set() method & \Lia\Addon\Cache::from($lia)->get() method
     */
    public function testMainCacheViaLiaMethod(){
        $main_file = 'main2.php';
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test1');
        $cache = new \Lia\Addon\Cache($package);
        $cache->init_lia();
        $cache->main = $main_file;
        $cache->dir = $this->cache_dir();
        // $cache->set('a.key', $target = 'some_value');
        \Lia\Addon\Cache::from($lia)->set('a.key', $target='some_value');

        $this->test('in memory');
        $this->compare(
            $target,
            $cache->get('a.key')
        );
        $this->compare(
            $target,
            \Lia\Addon\Cache::from($lia)->get('a.key')
        );

        $this->test('from disk');
        $cache->write();


        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test2');
        $cache = new \Lia\Addon\Cache($package);
        $cache->init_lia();
        $cache->dir = $this->cache_dir();
        $cache->main = $main_file;
        $cache->read();
        $cache->dir = $this->cache_dir();
        $this->compare(
            $target,
            $cache->get('a.key')
        );
        $this->compare(
            $target,
            \Lia\Addon\Cache::from($lia)->get('a.key')
        );

        unlink($cache->main_file_path());

    }
    /**
     *
     * @tests caching via $lia->set('cache.a.key') & $lia->get('cache.a.key');
     * @tests $cache->get('a.key'); as well
     */
    public function testMainCacheViaLiaSet(){
        $main_file = 'main2.php';
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $cache = new \Lia\Addon\Cache($package);
        $cache->main = $main_file;
        $cache->dir = $this->cache_dir();
        // $cache->set('a.key', $target = 'some_value');
        //$lia->set('cache.a.key', $target='some_value');
        \Lia\Addon\Cache::from($lia)->set('a.key', $target='some_value');

        $this->test('in memory');
        $this->compare(
            $target,
            $cache->get('a.key')
        );
        $this->compare(
            $target,
            \Lia\Addon\Cache::from($lia)->get('a.key')
        );

        $this->test('from disk');
        $cache->write();


        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test2');
        $cache = new \Lia\Addon\Cache($package);
        $cache->dir = $this->cache_dir();
        $cache->main = $main_file;
        $cache->read();
        $cache->dir = $this->cache_dir();
        $this->compare(
            $target,
            $cache->get('a.key')
        );
        $this->compare(
            $target,
            \Lia\Addon\Cache::from($lia)->get('a.key')
        );

        unlink($cache->main_file_path());

    }

    /**
     * Uses $cache->set('a.key') & $cache->get('a.key');
     * @tests standalone caching with just the addon.
     * @tests $cache->write() & $cache->read();
     */
    public function testMainCache(){
        $main_file = 'main1.php';

        $cache = new \Lia\Addon\Cache(null);
        $cache->main = $main_file;
        $cache->dir = $this->cache_dir();
        $cache->set('a.key', $target = 'some_value');
        $this->test('in memory');
        $this->compare(
            $target,
            $cache->get('a.key')
        );

        $this->test('from disk');
        $cache->write();


        $cache = new \Lia\Addon\Cache(null);
        $cache->dir = $this->cache_dir();
        $cache->main = $main_file;
        $cache->read();
        $cache->dir = $this->cache_dir();
        $this->compare(
            $target,
            $cache->get('a.key')
        );

        unlink($cache->main_file_path());

    }


    protected function cache_dir(){
        return dirname(__DIR__,2).'/input/Cache';
    }
}

