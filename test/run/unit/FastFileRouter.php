<?php

namespace Lia\Test;

class FastFileRouter extends \Tlf\Tester {

    /**
     * used to generate a mimetype map from array into one file for each extension
     */ 
    public function testMakeExtMap(){
        echo "This test is used just to generate the mime types text files. If they ever need to be re-generated, then re-enable this test";
        $this->disable();
        return;
        $mimetypes = require(__DIR__.'/../../../code/file/mime_type_map.php');

        // print_r($mimetypes);

        $dir = __DIR__.'/../../../code/file/mime_types/';
        $mimes = [];
        foreach ($mimetypes['mimes'] as $ext=>$mimetypes){
            $mimes[$ext] = $mimetypes[0];
            file_put_contents($dir.$ext.'.txt',$mimetypes[0]);
        }
        // $len = strlen($ext);
        // sort($nums);
        print_r($mimes);
    }

    public function testCssFile(){
        $css = $this->get('/test.css');
        echo $css;
        $this->compare('.css{background:blue;}',$css);
    }

    public function testGetParams(){
        $response = $this->get('/getme/?bears=the%20cutest');
        echo $response;
        $this->compare('the cutest', $response);
    }

    public function testEndingSlash(){
        $response = $this->get('/fastroute/');
        echo $response;
        $this->compare('this answers `/fastroute/`', $response);

    }

    public function testDotDot(){
        $response = $this->get('/fastroute../?okay');
        echo $response;
        $this->compare('this answers `/fastroute/`', $response);
    }

    public function testFastRoute(){
        $response = $this->get('/fastroute/');
        echo $response;
        $this->compare('this answers `/fastroute/`', $response);

    }

}
