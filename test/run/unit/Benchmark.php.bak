<?php

namespace Lia\Test\Unit;

class Benchmark extends \Tlf\Tester {

    public $loop_count = 100000;

    public function testInterfaceObjectUsingCall(){
        $obj = new UsesObject();
        $dep = new UsesObjectDependency();
        $obj->dependency = $dep;

        $i = 0;
        while ($i++<$this->loop_count){
            $out = $obj->call_stuff($i);
        }

    }

    public function testInterfaceObject(){
        $obj = new UsesObject();
        $dep = new UsesObjectDependency();
        $obj->dependency = $dep;

        $i = 0;
        while ($i++<$this->loop_count){
            $out = $obj->do_stuff($i);
        }

    }

    public function testArrayMethods(){
        $obj = new UsesMethodsArray();
        $dep = new UsesObjectDependency();
        $obj->methods['abc'] = [$dep, 'abc'];
        $obj->methods['def'] = [$dep, 'def'];

        $i = 0;
        while ($i++<$this->loop_count){
            $out = $obj->do_stuff($i);
        }
    }

    public function testArrayMethodsWithFunctions(){
        $abc = function(int $i){
                    if ($i%2==0)return 'abc';
                    else return 'cba';
                };
        $def = function(int $i){
                    if ($i%2==0)return 'def';
                    else return 'fed';
                };

        $obj = new UsesMethodsArray();
        $obj->methods['abc'] = $abc;
        $obj->methods['def'] = $def;

        $i = 0;
        while ($i++<$this->loop_count){
            $obj->do_stuff($i);
        }

    }

    public function testInstantiateInterfaceObject(){
        $i = 0;
        while ($i++<$this->loop_count){
            $obj = new UsesObject();
            $dep = new UsesObjectDependency();
            $obj->dependency = $dep;
        }
        
    }

    public function testInstantiateArrayMethods(){
        $i = 0;
        while ($i++<$this->loop_count){
            $obj = new UsesMethodsArray();
            $dep = new UsesObjectDependency();
            $obj->methods['abc'] = [$dep, 'abc'];
            $obj->methods['def'] = [$dep, 'def'];
        }
    }

    public function testInstantiateArrayMethodsWithFunctions(){
        $i = 0;
        while ($i++<$this->loop_count){
            $obj = new UsesMethodsArray();
            $dep = new UsesObjectDependency();
            $obj->methods['abc'] = 
                function(int $i){
                    if ($i%2==0)return 'abc';
                    else return 'cba';
                };
            $obj->methods['def'] = 
                function(int $i){
                    if ($i%2==0)return 'def';
                    else return 'fed';
                };
        }

    }
}


class UsesObject {
    
    public UsesObjectDependency $dependency;

    public function do_stuff(int $i){
        $abc = $this->dependency->abc($i);
        $def = $this->dependency->def($i);
        return $abc . '-' . $def;
    }

    public function call_stuff(int $i){
        $abc = $this->abc($i);
        $def = $this->def($i);
    }

    public function __call(string $method, array $args){
        return $this->dependency->$method(...$args);
    }
}

interface UsesObjectDependencyInterface {
    public function abc(int $i);
    public function def(int $i);
}

class UsesObjectDependency implements UsesObjectDependencyInterface {

    public function abc(int $i){
        if ($i%2==0)return 'abc';
        else return 'cba';
    }
    public function def(int $i){
        if ($i%2==0)return 'def';
        else return 'fed';
    }
}

class UsesMethodsArray {

    public array $methods = [];

    public function do_stuff(int $i){
        $abc = $this->abc($i);
        $def = $this->def($i);
        return $abc . '-' . $def;
    }

    public function __call(string $method, array $props){
        return $this->methods[$method](...$props);
    }
}
