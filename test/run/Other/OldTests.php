<?php

namespace Lia\Test;

/**
 * To keep old tests that I used to help write features.
 */
class OldTests extends \Tlf\Tester {


    /**
     * The flow of how scanning & prefixes work. This is all abstracted away now and internals can change.
     *
     * This test will not be maintained
     *
     */
    public function testPrefixScanning(){
        require_once($this->cli->pwd.'/test/input/Hook/Sample.php');
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test:prefixscanning');
        $hooks = new \Lia\Addon\Hook($package);
        $sample = new \Lia\Test\Addon\Hook\Sample($package);
        $package->init_lia();

        $prefix_handlers = [
            'on' => [$hooks, 'prefix_method_added']
        ];

        $prefix_methods = [];
        $methods = get_class_methods($sample);

        foreach ($prefix_handlers as $prefix=>$handler){
            $len = strlen($prefix);
            foreach ($methods as $m){
                if (substr($m,0,$len)==$prefix){
                    $dot_name = strtolower(substr($m,$len));
                    $dot_name = str_replace('_','.',$dot_name);
                    $handler($sample, $m, $dot_name);
                }
            }
        }

        ob_start();
        $sample->onHook_Test();
        $target = ob_get_clean();

        ob_start();
        $hooks->call('hook.test');
        $actual = ob_get_clean();

        $this->compare(
            $target,
            $actual
        );
    }
}
