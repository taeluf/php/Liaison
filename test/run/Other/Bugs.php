<?php

namespace Lia\Test;

/**
 * for testing bugs I find while working on Liaison
 */
class Bugs extends \Tlf\Tester {


    /**
     * @bug(dec-16-2021) the 'lia' object being passed to views is NOT the root lia object. Could not re-produce issue in test. This bug originated in taeluf.com
     */
    public function testViewLiaObj(){
        $_SERVER['REQUEST_URI'] = '/';
        $lia = new \Lia();
        $server = new \Lia\Package\Server($lia);
        $test = new \Lia\Package\Server($lia, 'test', $this->file('test/Server/'));

        $target = get_class($lia).'#'.spl_object_id($lia).'#addon-count('.count($lia->addons).')';
        $actual = \Lia\Addon\View::from($lia)->view('wrong-lia').'';

        $this->compare($target, $actual);
    }


    /**
     * Happening on taeluf.com. I have a `theme.php` view & associated `.css` and `.js` files in `theme/` dir. I think the files are all being added to `resources` addon, BUT they're NOT being output to the cached css file
     *
     * This test FAILED to re-create the bug. So I realized that resource files were being added AFTER the theme view was being `require`d. I moved the code around & now it works. I have not added a test for this.
     *
     * @tldr theme view wasn't adding resources. This test fails to test that. The issue is fixed & untested.
     */
    public function testViewAddsChildResources(){
        $this->empty_dir($this->file('test/Server/cache/'));

        $response = $this->get('/nest-res/');
        preg_match("/href=\"([^\"]+)\"/", $response, $css_matches);
        $css_url = $css_matches[1];
        preg_match("/src=\"([^\"]+)\"/", $response, $js_matches);
        $js_url = $js_matches[1];

        $css = $this->get($css_url);
        $js = $this->get($js_url);

        $this->test('compiled css');
        echo "\n\n".$css;
        $this->str_contains($css, ['.one{}','.two{}']);
        $this->test('compiled js');
        echo "\n\n".$js;
        $this->str_contains($js, ['var one;','var two;']);


        $this->empty_dir($this->file('test/Server/cache/'));
    }


}
