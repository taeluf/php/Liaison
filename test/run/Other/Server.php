<?php

namespace Tlf\Tester\Test;

/**
 * Test the fully-functional server
 */
class Server extends \Tlf\Tester {

    public function testDynamicUrl(){
        $out = $this->get('/dynamic/babybears/?no_theme=true');

        echo $out;
        $this->compare('babybears', $out);
    }

    /**
     * @bug(dec-15-2021) all my requests are delivering to the built-in liaison theme, not to the theme for the site. I noticed this issue in taeluf.com
     * @option 1: always overwrite the null-namespace when adding a new view
     * @option 2: Add an overwrite($view_name, $with_namespace) function 
     * @option 3: Find a way to instantiate the SITE before the SERVER
     * @option 4: Add a config for overwriting null-namespace when adding a new view
     * @option 5: add a 3rd paramater to addView() functions accepting true/false to overwrite/not
     * @option 6: Config in Server ADDON to choose what view to use as theme
     *
     * @solution option 1, always overwrite null-namespace when adding a new view. If you want the other view, it can be used explicitly. 
     */
    public function testUsesCorrectTheme(){
        $response = $this->get('/');

        echo $response;
        $this->str_contains($response, '<!-- TEST THEME -->');


    }

    /**
     * @bug(dec-15-2021) delivering static files works, but doing so through the Resources class does not. Getting a class 'Lia\Exception' not found in StaticFile.php line 104. I removed the Lia\Exceptions for \Exceptions. staticFile is receiving a file path that ends with '.file' because that's how I re-designed the cache class. I added a `$type` param to the static file class constructor & made minor modifications so mime-type is derived from this $type param, if given.
     */
    public function testCachedCssFile(){
        $this->empty_dir($this->file('test/Server/cache/'));

        $url = $this->get('/add-resource-file/');
        echo "\nUrl for resource file is: $url\n";
        $css = $this->get($url);

        echo $css;

        $this->str_contains($css,'.css{background:blue;}');

        $this->empty_dir($this->file('test/Server/cache/'));
    }

    public function testStaticFile(){
        $response = $this->get('/sample.css');

        echo $response;
        $this->str_contains(
            $response,
            'background: fuchsia'
        );
    }

    /**
     * @test a full request to the home page
     */
    public function testServer(){
        $response = $this->get('/');

        $this->str_contains(
            $response,
            [
                '<!DOCTYPE html>',
                'index page',
                '</html>',
            ]
            );
    }
}

