<?php


namespace Lia\Test;

class BasePackage extends \Tlf\Tester {

    /**
     * Test the Package's integration with addons. 
     * @howto(Test another addon) In `test/input/AppPackage` add a new class that extends from the real addon. Then write some simple test code to ensure integration with the package.
     *
     * @note this is NOT a test of the addons themselves.
     *
     * @note nov 18 2021: this is a sample of using the base package to craft a unified environment in a functional manner. There will be a Server package that ... basically wraps all of this up. First, I will probably write another test that does these same things, but via package hooks or something? Like using $addon->package_ready($package_dir) or something instead of scripting each addon's setup here. It's hard to tell rn
     *
     */
    public function testPackageAddonIntegration(){

        ///// 
        // # Ideas:
        // $package->dir('relative_path'); to get $package->dir.'/relative_path/'
        //
        // $addon->package_ready(); get called by $package->package_ready();
            // in server package, the server package will call package_ready() so user-land code doesn't have to worry abou this.
            // maybe create another package subclass to include some of these basic features that are not server-specific
        //
        // $addon->package_added() (or something) for first-hand initiation (though __construct can be overridden to achieve this. So it might just be silly fodder.
        //
        //  depends feature for addons, so they get initiated in the right order


        // make the base package
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test:packageaddonintegration');
        $package->dir = $this->package_dir();
        $package->load_addons('addon');
        $package->init_lia();

        /////
        // autoload addon
        /////
        $package->addons['autoload']->enable();
        \Lia\Addon\Autoload::from($lia)->addDir($package->dir.'/class', 'Lia\Test\Package');

        $class_name = "Lia\\Test\\Package\\Somethin";
        $somethin = new $class_name();
        $this->compare($class_name, get_class($somethin));

        /////
        // cache addon
        /////
        // $package->not_a_method();
        \Lia\Addon\Cache::from($lia)->dir = $package->dir('cache');
        \Lia\Addon\Cache::from($lia)->set('a.key', 'value');
        $unfile = $package->addons['cache']->main_file_path();
        if (is_file($unfile))unlink($unfile);
        \Lia\Addon\Cache::from($lia)->write();
        \Lia\Addon\Cache::from($lia)->read();
        $this->compare('value', \Lia\Addon\Cache::from($lia)->get('a.key'));

        $unfile=$package->addons['cache']->cache_file_path('abc');
        if (is_file($unfile))unlink($unfile);
        \Lia\Addon\Cache::from($lia)->cache_file('abc', 'some-value');
        $this->compare('some-value', \Lia\Addon\Cache::from($lia)->get_cache_file_content('abc'));

        /////
        // hook addon
        /////
        \Lia\Addon\Hook::from($lia)->add('a.thing', function(){return 'ok';});
        $ret = \Lia\Addon\Hook::from($lia)->call('a.thing');
        $this->compare(['ok'], $ret);
        

    }


    public function package_dir(){
        return $this->cli->pwd.'/test/input/AppPackage';
    }

}
