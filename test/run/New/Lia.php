<?php

namespace Lia\Test\NewTest;

class Lia extends \Tlf\Tester {


    public function testAddonFromLia(){
        $_SERVER['REQUEST_URI'] = '/';

        $lia = new \Lia();
        // Add the built-in App, which provides all the web-server features.    
        $server_package = new \Lia\Package\Server($lia, $fqn='lia:server');  // dir & base_url not required

        $res_addon = \Lia\Addon\Resources::from($lia);

        $this->compare(\Lia\Addon\Resources::class, get_class($res_addon));
    }

}
