<?php

namespace Lia\Test\Addon\Hook;

class Sample extends \Lia\Addon {

    public string $fqn = 'test:meh.sample';

    public function onHook_Test(){
        echo "hook test";
    }
}
