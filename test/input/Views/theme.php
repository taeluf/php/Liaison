<!DOCTYPE html>
<html>
    <head>
        <?='';//\Lia\Addon\View::from($lia)->view('lia/Resources');?>
        <?=\Lia\Addon\Resources::from($lia)->getHtml();?>
    </head>
    <body>
        <header>
            <h1>Header</h1>
        </header>
        <main>
            <?php if (\Lia\Addon\Error::exists($lia))echo \Lia\Addon\Error::from($lia)->headerMessage; else echo "OHNO"?>
            <?=''//\Lia\Addon\View::from($lia)->view('lia/Content'); ?>
            <?=$content?>
        </main>
        <footer>
            <h3>Footer</h3>
        </footer>
    </body>
</html>
