<div class="blog">
    <h1><?=$title?></h1>
    <p><?=$summary?></p>
    <?php
    
    $body = explode("\n", $body);
    $body = '<p>' . implode('</p><p>', $body) . '</p>';

    ?>
    <div class="BlogContent">
        <?=$body?>
    </div>
</div>