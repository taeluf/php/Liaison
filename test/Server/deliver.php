<?php

require(__DIR__.'/../../code/class/Router/FastFileRouter.php');
\Lia\FastFileRouter::php(__DIR__.'/fastroute/',[]);
\Lia\FastFileRouter::file(__DIR__.'/file/');


require_once(dirname(__DIR__,2).'/vendor/autoload.php');

try {


$lia = new \Lia();

// $lia->props['site']['dir'] = __DIR__;
// $lia->props['server']['cache']['dir'] = __DIR__.'/cache/';

$main = new \Lia\Package\Server($lia, 'server');
$test = new \Lia\Package\Server($lia, 'site', __DIR__);


if (($_GET['no_theme']??null)=='true'){
    //$lia->set('lia:server.server.useTheme', false);
    \Lia\Addon\Server::from($lia)->useTheme = false;
}


\Lia\Addon\Server::from($lia)->deliver();

} catch (\Exception $e){
    throw $e;
}
