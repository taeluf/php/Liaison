# Markdown Support
Markdown support is not built into Liaison, and requires third-party software to do the Markdown to HTML conversion. You can use CommonMark, Parsedown, or any other package to do this, in conjunction with Liaison's Hooks, in order to add Markdown support to views and public files. 

Alternatively, you can do markdown conversions manually within each view or public file where you use markdown.

**Related:** @see_files(/docs/Hooks.md; Hooks, /docs/Packages.md; Packages [bootstrap], /docs/Views.md; Views, /docs/Routes.md; Routes, https://commonmark.thephpleague.com/; CommonMark, https://parsedown.org/; Parsedown)

## Docs
- Public Files 
- Views

### Public Files / Routes
Markdown support can be added to public routes by using one hook to modify URLs and another hook to modify content & convert output to HTML from Markdown.

This can go in the `bootstrap.php` for your package. 
`bootstrap.php`:  
```php
<?php
/** @var $this \Lia\Package the package this bootstrap file belongs to */

$my_package = $this; 

// Modify Routes to remove `.md` from `.md.php` files. You might want to also remove `.md` from plain `.md` files or replace it with `.html`.
\Lia\Addon\Hook::from($this->lia)->add(\Lia\Hooks::PACKAGE_ROUTE_PATTERNS_LOADED,
    /** 
    * Returns an array of modified patterns like `array<string rel_file_name, string url>`, where `.md` is removed from URLs for public files ending in `.md.php`.
    */
    function(Lia\Package\Server $package, array $array_of_patterns) use ($my_package) : array {
        // only do the conversions for your own package
        if ($package !== $my_package) return $array_of_patterns;

        foreach ($array_of_patterns as $rel_file=>$url_pattern){
            // `.php` was already removed and replaced with a trailing slash
            if (substr($url_pattern,-4)=='.md/'){ 
                // remove `.md/` and re-add the `/`
                $new_pattern = substr($url_pattern,0,-4).'/'; 
                // remove `/index/`, but keep a slash at the end
                if (substr($new_pattern,-7) == '/index/')$new_pattern = substr($new_pattern,0,-6); 
                $array_of_patterns[$rel_file] = $new_pattern;
            }
        }

        return $array_of_patterns;
    }
);

// convert the route's output from markdown to HTML 
\Lia\Addon\Hook::from($this->lia)->add(\Lia\Hooks::ROUTE_RESOLVED,
    function(\Lia\Obj\Route $route, \Lia\Obj\Response $response) use ($my_package){
        if (!$route->isFile())return; 

        // only do the conversions for your own package
        if ($route->package() !== $my_package) return;

        $target = $route->target();
        if (substr($target, -7) != '.md.php'
            && substr($target, -3) != '.md'
        ){
            // The target file is not a `.md` or `.md.php` file, so return.
            return;
        }
    
        // must be installed separately. CommonMark is another option.
        $Parsedown = new Parsedown();
        $response->content = $Parsedown->text($respones->content);
    }
);
```

### Views
Markdown support can be added to views by using the `VIEW_LOADED` hook to convert MD to HTML for view files ending in `.md.php`.
**@Note:** Only one `VIEW_LOADED` hook is allowed across a Liaison instance. This will likely be changed in the future. (*2025-02-02*)


This can go in the `bootstrap.php` for your package. 
`bootstrap.php`:
```php
<?php
/** @var $this \Lia\Package the package this bootstrap file belongs to */

// Typically, the package's name is the same as the view's namespace.
$my_namespace = $this->name; 

\Lia\Addon\Hook::from($lia)->add(\Lia\Hooks::VIEW_LOADED,
    function(?string $namespace, string $view_name, array $view, string $content) use ($my_namespace): string {
        if ($namespace!=$my_namespace) return $content;
        // View FILES all end in `.php`, but `.php` is NOT part of view NAMES
        if (substr($view_name,-3) != '.md')return $content;

        $Parsedown = new Parsedown();
        return $parsedown->text($content);
    }
);
```
