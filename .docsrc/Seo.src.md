# SEO - Search Engine Optimization
The Seo addon simplifies the addition of meta tags that search engines and social media sites need, like the `og:image` and `og:description` tags, as well as the `<title>` on the page. Any other HTML can be also be added via this addon to the `<head>` of the page. 

The Resources addon produces its own `<head>` html for scripts and stylesheets and collects html from the SEO addon. The Theme view will get this html from the resources addon and print it.

**Related:** @see_files(/docs/Resources.md; Resources, /docs/Views.md; Views, /docs/api/code/addon/Seo.php.md; Seo Addon)

## Docs
- Convenience Methods
- Seo tags from array
- Arbitrary `<head>` html

## Convenience Methods
Call any or all of the following methods to setup the meta tags for a page. Be sure to use full URLs, as some search engines and social media sites require this.
```php
<?php
@import(Seo.MainMethods)
```

**Output:**
```html
@import(Seo.MainMethodsOutput)
```

## Seo tags from array
**From Array:**
You can also setup seo data from an array, which will call each of the appropriate methods above. (*the array key is the method name*)
```php
<?php
@import(Seo.FromArray)
```

The output is the same as calling the methods directly.

## Arbitrary HTML & Meta tags
- `\Lia\Addon\Seo::from($lia)->addHeadHtml('<arbitrary html>');` - add any html you want to the `<head>`
- `\Lia\Addon\Seo::from($lia)->meta("example", "information");` - Add an arbitrary `<meta>` tag to the `<head>` like `<meta name="example" content="information">`
