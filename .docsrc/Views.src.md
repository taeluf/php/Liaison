# Views
Views are php scripts loaded by the View addon that typically produce HTML output, and may have associated `css` and `js` files. Your `Theme` view will call `\Lia\Addon\Resources::from($lia)->getHtml()` to automatically include those resource files on the page. View scripts receive an array of arguments from the calling PHP code.

Markdown support can be added via Hooks. Views are typically files, organized in the `view` dir of your package. View can also be added manually with any file target or as a callable.

**Related:** @see_file(/docs/Theme.md, Theme), @see_file(/docs/Markdown.md, Markdown Support), @see_file(/docs/Resources.md, Resources [CSS & Javascript]), @see_file(/docs/api/code/addon/View.php.md, Views Addon), @see_file(/docs/Packages.md, Packages)

## Docs
- Display a view
- Create a view
- CSS & Javascript Files
- Add Views Manually
- Replace an existing view
- Callable Views

## Display a view
Views are displayed by calling `\Lia\Addon\View::view('namespace:viewname', [...])`. The `namespace` is the the package's namespace (*no vendor*) followed by the view's name (*with the `.php` extension removed if it's a file.*)

Example of a public file loading a view.
`public/show-bear-names.php`:
```php
<?php

$bear_names = ['sasha', 'strawbeary', 'peter'];

$va = \Lia\Addon\View::from($lia);

// Loads the view file `view/bear-names-list.php` in the package `reedybear:fun-bear`
echo $va->view('fun-bear:bear-names-list', ['bearnames'=>$bear_names]);
```

## Create a view
Views are just simple PHP scripts that typically output HTML. They are called from within the `View` addon, so `$this` refers to an instance of `\Lia\Addon\View`.

Example of a view.
`Site/view/bear-names-list.php`
```php
<?php
/**
*
* @param $this \Lia\Addon\View the View addon instance that is loading this view
* @param $view array view information, like the view `'type'=(main, file, or callable)` AND `name=bear-names-list && dir=ABSOLUTE_PATH` or `file=ABSOLUTE_PATH` or `callable=function(){...}` 
*/
<h1>Bear Names</h1>
<ul>
<?php foreach ($bearnames as $index => $name): ?>
<li><a href="/bear/<?=$name?>/"><?=$name?></a></li>
<?php endforeach; ?>
</ul>
```

To load the package for the current view:
```php
<?php
...
</ul>

$package = $this->packages[$view['name']];
```

**Note:** This package-loading mechanism is highly flawed, will be changed, and currently may load the wrong package if there are same-named views in different packages. (*2025-02-02*)

## CSS & Javascript Files
CSS & Javascript files are loaded automatically for `main` views - views within your `view` dir, added via `addDir()` or via `addView()`. 

Files with the `.css` or `.js` extension that share a name with the view file are loaded. `.css` and `.js` files in the same-named sub-directory are also loaded (*1 deep*). The next level subdirectory (*2 deep*) is not loaded.

Views added via `addViewFile()` and `addViewCallable()` do not have any resource files automatically loaded.

Example directory structure
Displaying `bear-fun:bear-names-list`:
```
Site/
    public/ ...
    view/
       bear-names-list.php # The view being loaded
       bear-names-list.css # added automatically
       bear-names-list.js # added automatically
       bear-names-list/ # descended into
           fancy-list.css # added automatically
           animated-bear-names.css # added automatically
           additional-scripts.js #  added automatically
           
           bear-list-item.php # NOT loaded 
           bear-list-item/ # NOT descended into
               bear-list-item.css # NOT added 
               bear-list-item.js # NOT added 
```

## Add Views Manually
Views can be added 5 different ways.
1. `view/` dir: `.php` files in the `view/` dir of your package are automatically loaded.
2. `addDir(string dir, \Lia\Package $package)`: loads views within a directory. (*css & js files are automatically added*)
3. `addView(string $view_name, string $dir)`: Add a single view with support for css & js file loading. Do NOT include the `.php` extension. DO include a namespace in `$view_name`, like `fun-bear:bear-names` for `dir/bear-names.php`.
4. `addViewFile(string $view_name, string $file)`: Add a single view. `$view_name` like `"namespace:name"`. `$file` is an absolute path. css & javascript files are NOT loaded automatically.
5. `addViewCallable(string $view_name, mixed $callable)`: Add a single view pointing to a callable. `$view_name` like `namespace:name`. css & js files are NOT loaded.

## Replace an existing view
You might want to replace a view created by another package. Perhaps you don't like the overly simple `fun-bear:bear-names-list` view. To overwrite it, simply add a view using any of the mechanisms above, providing an identical view name, like `addViewFile('fun-bear:bear-names-list', $path_to_file)`. 

Now, any time this view is loaded, by any package, your version of the view will be loaded instead. The original css & javascript files will not be loaded, but you can add them manually using the @see_file(/docs/Resources.md, Resources Addon).

## Callable Views
Callable views receive their own view name and an array of arguments and must return a string.
```php
<?php
@import(Views.add_callable)
```
