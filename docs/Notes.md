<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Notes in this project's code  
Notes are marked in code by writing `@NOTE`. Everything on the line after it is captured.  
[line number] Trimmed Text of the line  
  
## Notes for code/class/Router/FastFileRouter.php  
[Docs](/api/code/class/Router/FastFileRouter.php.md)  
[Source Code](/code/class/Router/FastFileRouter.php)  
  
- [35] does not send php files ...  
  
## Notes for code/class/Server.php  
[Docs](/api/code/class/Server.php.md)  
[Source Code](/code/class/Server.php)  
  
- [94] I'm adding dir `/theme/` as a view dir in Server package setup, and I don't know why. I should remove this probably.  
- [107] old 'setup_public_routes()' method has been disabled. It had an error & doesn't appear to be used  
  
## Notes for code/class/Addon.php  
[Docs](/api/code/class/Addon.php.md)  
[Source Code](/code/class/Addon.php)  
  
- [86] Addon::init_lia() is called with paramaters, but not defined with them, because existing addons are already implementing the naked version of this method. This will surely be updated in the future, likely with a different method name.  
- [97] Addon::onPackageReady() is called with paramaters, but not defined with them, because existing addons are already implementing the naked version of this method. This will surely be updated in the future, likely with a different method name.  
  
## Notes for code/class/Package.php  
[Docs](/api/code/class/Package.php.md)  
[Source Code](/code/class/Package.php)  
  
- [62] subclass `\Lia\Package` or `\Lia\Package\Server` and implement a custom 'bootstrap()' method.  
  
## Notes for code/addon/Router.php  
[Docs](/api/code/addon/Router.php.md)  
[Source Code](/code/addon/Router.php)  
  
- [11] `$varDelim` is a string of characters used to separate dynamic portions of a url.  
- [12] `$varDelim` is a global setting, but you can change it, add routes, change it, add routes, etc.  
- [13] `pattern` refers to an unparsed url pattern like `/{category}/{blog_name}/`  
- [14] added routers take a `\Lia\Obj\Request` object  
- [15] route derivers accept `false` and return array of patterns or a `\Lia\Obj\Request` object  
- [16] test patterns (or testReg) are simplified patterns with `?` in place of dynamic paramater names & are used internally  
- [17] `$routeMap` contains all the info needed for selecting which route to use  
- [18] optional paramaters (`/blog/{?optional}/`) only allow for one optional paramater  
- [28] this setting is global across all routes. You can circumvent this by using a second router addon  
- [75] '.php' extension is removed when converting a file path to a url pattern  
- [79] if `.php` extension is removed, then a trailing slash is added. This is a bug and may be fixed in the future.  
- [84] 'index' filename is removed when converting a file path to a url pattern  
- [93] if there is no file extension in the final pattern, then a trailing slash is added (if .php extension is removed, trailing slash is also added, even if there is another extension. This is a bug and may be fixed in the future.)  
- [97] `///` and `//` are replaced with a single `/`  
- [275] docblock not done for addDirectoryRoutes()  
  
## Notes for code/addon/Cache.php  
[Docs](/api/code/addon/Cache.php.md)  
[Source Code](/code/addon/Cache.php)  
  
- [137] Does not check if cache is valid  
- [150] Does not check if cache is enabled, existence of file or meta file, or expiration of file  
- [164] This implementation is likely inefficient (not benchmarked)  
- [165] Creates the cache directory if it does not exist. This feature may be removed in the future  
  
## Notes for code/addon/Resources.php  
[Docs](/api/code/addon/Resources.php.md)  
[Source Code](/code/addon/Resources.php)  
  
- [37] not currently implemented  
- [38] $_GET['true'] or $_GET['false'] will change this setting  
- [68] (Backburner) use new implementation for delivering cached Resource files  
  
## Notes for code/addon/Server.php  
[Docs](/api/code/addon/Server.php.md)  
[Source Code](/code/addon/Server.php)  
  
- [18] Nov 30, 2021: Not currently used  
- [26] $response->useTheme must ALSO be `true` for theme to be used.  
- [105] if `$_GET['theme']=='json'`, response will be returned as json. Idr the format exactly.  
- [179] any public file route not ending in `.php` will set `$response->useTheme = false;` and add static file & cache headers.  
- [247] Nov 30, 2021: Not currently in use  
- [262] info about which paramaters are passed to public file route  
  
## Notes for code/addon/View.php  
[Docs](/api/code/addon/View.php.md)  
[Source Code](/code/addon/View.php)  
  
- [170] (jan 17, 2022) only scans cur_level ... does not descend  
  
## Notes for code/addon/Seo.php  
[Docs](/api/code/addon/Seo.php.md)  
[Source Code](/code/addon/Seo.php)  
  
- [43] Each key corresponds to a method on this class  
- [44] Each array value will be expanded as multiple paramaters  
- [45] Each string value will be used as a single paramater  
  
## Notes for code/addon/Redirect.php  
[Docs](/api/code/addon/Redirect.php.md)  
[Source Code](/code/addon/Redirect.php)  
  
- [7] addon_name is 'goto'  
  
## Notes for test/run/Other/BasePackage.php  
[Docs](/api/test/run/Other/BasePackage.php.md)  
[Source Code](/test/run/Other/BasePackage.php)  
  
- [12] this is NOT a test of the addons themselves.  
- [14] nov 18 2021: this is a sample of using the base package to craft a unified environment in a functional manner. There will be a Server package that ... basically wraps all of this up. First, I will probably write another test that does these same things, but via package hooks or something? Like using $addon->package_ready($package_dir) or something instead of scripting each addon's setup here. It's hard to tell rn  
  
## Notes for test/run/Addon/Seo.php  
[Docs](/api/test/run/Addon/Seo.php.md)  
[Source Code](/test/run/Addon/Seo.php)  
  
- [60] if the underlying html changes, just copy+paste output from the test & copy it into the HTML HEREDOC  
  
## Notes for test/run/Addon/Router.Other.php  
[Docs](/api/test/run/Addon/Router.Other.php.md)  
[Source Code](/test/run/Addon/Router.Other.php)  
  
- [37] this test changes the varDelim