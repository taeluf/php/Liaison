<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Theme / Page Layout  
Your Theme is a special View defining the page-layout for the site. This includes the `<html>` tag, footer, and header content. Within the theme, you will print the page content, which is loaded via routing, and the head html is built from various addons to include resources (css & js) and SEO meta tags. You can use different themes (page layouts) for different parts of your site by changing the theme name. The Theme is loaded by the Server addon.  
  
**Related:** [Views](/docs/Views.md), [Resources](/docs/Resources.md), [Seo](/docs/Seo.md), [Server](/docs/Server.md), [Routes](/docs/Routes.md)  
  
## Docs  
- Tips  
- Sample Theme  
- JSON Output  
  
## Tips  
- Change the view name for your theme: `\Lia\Addon\Server::from($lia)->setTheme('namespace:view_name');`  
- Theme can be disabled by setting `\Lia\Addon\Server::from($lia)->useTheme = false` OR `$response->useTheme = false`.  
- Liaison provides a simple built-in theme for testing purposes.  
- `json` is returned if `$_GET['theme']=='json'`. To disable this feature, `unset($_GET['theme'])` during or before route processing, such as in your site's `deliver.php` script or during the `ROUTES_FILTERED` hook.  
  
## Sample Theme  
Your default theme is a view file named `theme.php`. When it is loaded, `theme.css` and `theme.js` will be loaded, automatically. `$content` is automatically exposed to your theme view & is loaded from the routing.  
  
`\Lia\Addon\Resources::from($lia)->getHtml()` will print script and stylesheet tags and SEO meta tags.  
  
Create `view/theme.php`:  
```php  
<!DOCTYPE html>  
<html>  
<head>  
    <?=\Lia\Addon\Resources::from($lia)->getHtml()?>  
</head>  
<body>  
    <?=$content?>  
</body>  
</html>  
```  
  
## JSON Output  
To return a `json` response for a standard request, set `$_GET['theme'] = 'json'` either manually in code or via the request (`?theme=json`). If your route outputs json for its content, then disable the theme with `\Lia\Addon\Server::from($lia)->useTheme = false` or `$response->useTheme = false`, so that only your content sends to the browser. `.json` files will always be sent as json.  
  
**Note:** Features around json responses may receive breaking changes. JSON headers are not set for these `$_GET['theme'] = 'json'` responses. (*2025-02-02*)  
  
With `$_GET['theme'] == 'json'`, the following is sent to the browser:  
```php  
<?php  
$output = [  
    'content'=>$response->content,  
    'scripts'=>$scripts,  
    'stylesheets'=>$styles,  
];  
```  
