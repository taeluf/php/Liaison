<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Objects/Route.php  
  
# class Lia\Obj\Route  
A simple object to hold route information for a matched route.   
  
Nothing documented here is enforced by this class. It's all passed TO it.   
If a different Routing component is used, it might not follow the currently documented usage in this class.  
See source code at [/code/class/Objects/Route.php](/code/class/Objects/Route.php)  
  
## Constants  
  
## Properties  
- `protected $paramaters = [];` Paramaters extracted from the url, based upon the route.  
Ex: `/blog/{slug}/` requested by `/blog/happy-cats/` would yield `['slug'=>'happy-cats']`  
- `protected $url;` The requeted URL without any GET params  
- `protected $method;` The request method, like GET, POST, etc  
- `protected $urlRegex;` The regex that captured the url  
- `protected $paramaterizedPattern;` The declared route, unparsed, like `/blog/{slug}/`  
- `protected $placeholderPattern;` A simplified pattern for the route-selection algorithm  
For declared route `/blog/{slug}/`, $placeholderPattern would be `blog/?/`  
- `protected $allowedMethods = [];` Array of methods that are valid for this route. Such as GET, PUT, POST, DELETE, etc  
- `protected $target;` The thing being routed to. A callable or an absolute file path  
- `protected $package;` The package that create the route which was matched.  
  
## Methods   
- `public function __construct($details)` Make a route  
Sorry, I'm not documenting this further.   
Usage is in Router component, in the route() method.  
- `public function part($index)` The piece of the url to return  
Ex url: `/part1/part2/part3.php`  
`part(0)` returns `part1`.   
- `public function param($key)` Get a named url paramater.  
Ex: for `/blog/{slug}/`, `param('slug')` returns the slug  
- `public function var($index)` Get a named url paramater by index (zero-based)  
- `public function package()`   
- `public function url()` return the requested url  
- `public function method()` return the requested method (GET/POST/etc)  
- `public function target()` Get the callable or file this route points to  
- `public function type()` get the type (is callable vs. is file)  
- `public function isCallable()` check if target is callable  
- `public function isFile()` check if target is a file  
- `public function fileExt()` get the file extension if target is a file  
- `public function paramaterizedPattern()` Get the target pattern for this route with {param} declarations  
- `public function placeholderPattern()` Get the target pattern for this route with placeholders (?)  
- `public function allowedMethods()` array of methods allowed for this route  
- `public function paramaterCount()` number of url paramaters  
- `public function paramaterNames()` names of url paramaters  
- `public function paramaters()` array of paramaters  
- `public function isStatic()` true if there are no paramaters in the route pattern  
- `public function isDynamic()` true if there are paramaters in the route pattern  
  
