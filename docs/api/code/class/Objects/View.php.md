<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Objects/View.php  
  
# class Lia\Obj\View  
A View, which passes properties to a template file and loads associated resources (.css/.js files). You must invoke `__toString()` (`''.$view`) or `$view->content()` or `$view->resources()` or nothing happens.  
  
See source code at [/code/class/Objects/View.php](/code/class/Objects/View.php)  
  
## Constants  
  
## Properties  
- `protected $name;` Relative path to the view without file extension. Ex: `view/theme/Header.php` is named `theme/Header`  
- `protected $dir;` The root directory for finding views  
- `public $args = [];` Arguments to pass to the template file  
- `protected $content = null;` Output from including the template file  
- `protected $resources = null;` Array of resources  
```php  
$resources = ['path/to/script.js', 'path/to/theme.css'];  
```  
- `public $lia = null;`   
  
## Methods   
- `public function __construct($name, $dir, $allArgs)`   
- `public function resources()` Load all css & js files in the view's directory  
  
- `public function content()` Load template file  
- `public function __toString()` shorthand for content()  
  
