<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Objects/Request.php  
  
# class Lia\Obj\Request  
A simple object to represent a web request  
See source code at [/code/class/Objects/Request.php](/code/class/Objects/Request.php)  
  
## Constants  
  
## Properties  
- `protected $data;` $_GET or $_POST params or null, depending on the request method.  
- `protected $url;` Url Path (no query string, no domain)  
- `protected $method;` The method, GET, POST, PUT, etc  
  
## Methods   
- `public function __construct($url=null, $methodnull)`   
- `public function url(): string` Get the url  
- `public function method(): string` Get the request method (GET, PUT, POST, etc)  
- `public function data(): array` Get the request data (from `$_GET` or `$_POST` or null)  
  
