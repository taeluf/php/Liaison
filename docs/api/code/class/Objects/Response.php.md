<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Objects/Response.php  
  
# class Lia\Obj\Response  
Mutable object to represent a web response  
See source code at [/code/class/Objects/Response.php](/code/class/Objects/Response.php)  
  
## Constants  
  
## Properties  
- `public $request;` Generally a \Lia\Obj\Request  
- `public $content = '';` The body to send  
- `public $useTheme;` Whether to apply a theme or not  
- `public $headers = [];` headers to send  
- `public $sendContent = true;` Whether to send content or not. Mark false if you're sending headers to load-from-cache  
  
## Methods   
- `public function __construct($request)`   
- `public function addHeader($header)`   
- `public function addHeaders($headers)`   
- `public function sendHeaders()`   
  
