<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Hooks.php  
  
# class Lia\Hooks  
A class to organize all hook names across the main package.  
This class started on Aug 22, 2024. Pre-existing hooks may get added into here, but I have no explicit plans to do that.  
New hooks hopefully will all be added here.  
See source code at [/code/class/Hooks.php](/code/class/Hooks.php)  
  
## Constants  
- `const PACKAGE_ROUTE_PATTERNS_LOADED = 'lia:package.route_patterns_loaded';`   
- `const REQUEST_STARTED = 'RequestStarted';`   
- `const VIEW_LOADED = 'lia:server.view_loaded';`   
- `const ROUTES_FILTERED = 'RoutesFiltered';`   
- `const ROUTE_RESOLVED = 'RouteResolved';`   
- `const ROUTES_FOUND = 'RoutesFound';`   
- `const FILTER_ROUTE = 'FilterRoute';`   
- `const RESPONSE_READY = 'ResponseReady';`   
  
## Properties  
  
## Methods   
  
