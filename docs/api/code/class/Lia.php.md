<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Lia.php  
  
# class Lia  
  
See source code at [/code/class/Lia.php](/code/class/Lia.php)  
  
## Constants  
  
## Properties  
- `public $addons = [];` array of addons with namespaces, like 'lia:server.router'  
- `public $packages = [];` packages where key is fqn, like 'lia:server'  
  
## Methods   
- `public function __construct()`   
- `public function addPackage(object $package, string $fqn)`   
- `public function addAddon(object $addon, string $fqn)` Add an addon by it's fully qualified name  
- `public function addon($fqn): \Lia\Addon` Get an addon by it's fully-qualified-name  
- `public function package($fqn): \Lia\Package` Get a package by it's fully-qualified-name  
- `public function dump_thing($thing)`   
- `public function dump($thing=null)` Dump a bunch of info about liaison. Methods. Addons. Properties.  
- `public function __map_array__($value)`   
  
