<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Package.php  
  
# class Lia\Package  
  
See source code at [/code/class/Package.php](/code/class/Package.php)  
  
## Constants  
  
## Properties  
- `public string $name;` example: package  
- `public string $fqn;` example: vendor:package  
- `public string $dir = null;` The root dir for the package  
- `public \Lia $lia;` Liaison object  
- `public array $addons = [];` array of addons  
- `protected array $config = [];` Configurations for the package  
  
## Methods   
- `public function __construct(\Lia $liaison, string $fqn, string $dir=null)`   
- `protected function bootstrap()` `require()` the bootstrap.php file in the package's root dir, if it exists.  
  
It's called after the Package class has been initialized, with name, dir, lia, and fqn set. Addons are not yet loaded.    
If you use addons, call '$this->load_addons()' in your bootstrap.php file  
- `public function init_lia()` foreach $addon, $addon->init_lia  
- `public function load_addons($sub_dir = 'addon')`   
- `public function ready()`   
- `public function dir(string $sub_dir): string` Return the absolute path to a directory inside your package  
- `public function config(string $key): mixed` Get a config's value or `null` if it is not set  
  
- `public function setConfig(string $key, intstringboolfloatarray $value): void` Set the config's value to a primitive value or array. Arrays should NOT contain objects, only that which can be expressed in JSON.  
  
  
