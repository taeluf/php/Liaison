<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Server.php  
  
# class Lia\Package\Server  
  
See source code at [/code/class/Server.php](/code/class/Server.php)  
  
## Constants  
  
## Properties  
- `public string $fqn = 'lia:server';`   
- `public string $name = 'server';`   
- `public array $public_file_params = [];` key=>value params to automatically `extract()` into public files. Each package has it's own list of params to extract, so params from one package will not be shared with public files in other packages.  
- `public $base_url = '';` Setting this directly does not work, bc routes are setup during the constructor.  
  
Instead do `$lia->set('package_name.base_url', '/base_url/');`  
  
## Methods   
- `public function url(string $path)`   
- `public function __construct($lia, $name='lia:server', $dirnull, $base_urlnull)`   
- `static public function main($lia)`   
- `static public function main_dir()`   
- `public function goto($url)`   
  
