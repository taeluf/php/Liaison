<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Router/FastFileRouter.php  
  
# class Lia\FastFileRouter  
  
See source code at [/code/class/Router/FastFileRouter.php](/code/class/Router/FastFileRouter.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `static public function php($dir,$args=[])` Very quickly deliver a php file by appending the url to a directory  
Request url must end with a forward slash `/`  
GET params are removed from url  
`..` is removed from url  
  
- `static public function file($dir, $url=null, $path_append'')` Very quickly deliver a file. Supports browser caching.  
Uses a very fast implementation for getting mimetypes. Each file extension has a `*.txt` file that contains its mimetype.  
  
- `static public function set_download_file_name($file_name)` Set the file name via headers.   
Literally `header("Content-Disposition: filename=\"$file_name\"", false);`  
  
- `static public function send_file($file_path, $path_append='')` send headers & output file  
- `static public function get_mime_type($file_path)`   
  
