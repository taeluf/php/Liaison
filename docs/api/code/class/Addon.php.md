<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Addon.php  
  
# class Lia\Addon  
  
See source code at [/code/class/Addon.php](/code/class/Addon.php)  
  
## Constants  
  
## Properties  
- `public string $fqn;` Fully qualified name for accessing this addon  
- `public string $name;` The short name of this addon  
- `public \Lia\Package $package;` Package  
- `public \Lia $lia;`   
  
## Methods   
- `static public function from(\Lia $lia): static` Get an instance of the addon class from the Liaison object based on the default `fqn` of the called class, or throw if it isn't on the lia object.   
  
- `static public function exists(\Lia $lia): bool` Check if an instance of the called addon class exists on the Liaison object, based on the default `fqn` of the called addon class.  
  
- `public function __construct(\Lia\Package $package=null)`   
  
