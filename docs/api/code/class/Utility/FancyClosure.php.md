<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Utility/FancyClosure.php  
  
# class Lia\Utility\FancyClosure  
Normalize callable usage & bind paramaters to a callable, like you can in javascript  
See source code at [/code/class/Utility/FancyClosure.php](/code/class/Utility/FancyClosure.php)  
  
## Constants  
  
## Properties  
- `public $bound;` array of paramaters to pass to the callable when invoked  
- `public $origCallable;` The source callable as it was passed to this class  
- `public $function  null;` An anonymous function, if the callable is an anonymous function  
- `public $class  null;` The fully qualified classname, if the callable is an array like `[$object || 'classname', 'methodname']`.  
- `public $object = null;` The object, if the callable is an array like `[$object, 'methodName']`  
- `public $funcName = null;` The function name, if the callable is an array like `[$object || 'classname', 'methodname']`  
- `public $isStatic = false;` true if the callable is a static class function  
- `public $isAnonymous = false;` true if the callable is an anonymous function  
  
## Methods   
- `public function __construct($callable, $boundParamaters)`   
- `public function __invoke(...$args)` Invoke the given callable with any bound paramaters  
  
