<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Utility/StaticFile.php  
  
# class Lia\Utility\StaticFile  
Build headers for static files like .js or .css or image files  
See source code at [/code/class/Utility/StaticFile.php](/code/class/Utility/StaticFile.php)  
  
## Constants  
  
## Properties  
- `protected $file;` The absolute file path  
- `protected string $type;` the intended file extension, in case $file has a weird naming convention  
- `protected $headers = [];` Array of headers  
- `protected $typeHeaders = [];` headers for the content type & content length  
- `protected $cacheHeaders = [];` headers for the cache-state  
- `public $userHasFileCached = false;` true if request headers indicate the file has been cached on the client  
  
## Methods   
- `public function __construct($file, $type=null)`   
- `public function getContentTypeHeaders()`   
- `public function getCacheHeaders()`   
- `public function getHeaders()`   
- `protected function getFileMimeType($filePath)` Get mime type of a file based on its extension  
  
