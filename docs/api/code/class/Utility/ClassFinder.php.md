<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Utility/ClassFinder.php  
  
# class Lia\Utility\ClassFinder  
Find a fully qualified classname in a file. (or for multiple files in a directory)  
See source code at [/code/class/Utility/ClassFinder.php](/code/class/Utility/ClassFinder.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `static public function classesFromDir($classDir)`   
- `static public function getClassFromFile($file)`   
- `--declaration-missing--`   
  
