<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Utility/Files.php  
  
# class Lia\Utility\Files  
Utility class to work with files & directories  
See source code at [/code/class/Utility/Files.php](/code/class/Utility/Files.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `static public function all($dir,$fPathsRelTo='', $endingWith'')` Get all files in a directory. Does not return directories  
  
- `static protected function load_all(string $dir, string $fPathsRelTo,string $endingWith, array $files = [])` optimized version of the function without string replaces  
  
