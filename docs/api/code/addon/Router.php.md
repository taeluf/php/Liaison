<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/addon/Router.php  
  
# class Lia\Addon\Router  
description  
  
See source code at [/code/addon/Router.php](/code/addon/Router.php)  
  
## Constants  
  
## Properties  
- `public string $fqn = 'lia:server.router';`   
- `public $varDelim = '\\.\\/\\-\\:';` A string of characters that can be used as delimiters for dynamic url portions  
- `public $routeMap = [];`   
- `public array $routers = [];` alternate routers to use. Should only contain callables. The callables should accept `\Lia\Obj\Request` objects  
  
## Methods   
- `public function init_lia()`   
- `public function dir_to_patterns($dir, $prefix='')` Get an array of patterns from files in a directory.  
  
- `public function fileToPattern($relFile)` Convert a relative file path into a pattern  
  
- `public function clean_url($dirty_url)` Clean a url (without domain): replace space with '+', single quote with '%27', and replace multiple slashes with a single slash  
  
- `public function handle_route_prefix_method($object, $m, $dot_name)` Get routes by calling the object's method & use the method as the router.  
  
- `public function addRoute($pattern, $callbackOrFile,$package=null)` Add a route  
  
- `public function removeRoute(string $pattern): int` Remove all routes matching the target pattern.  
  
- `public function has_static_route(string $path,string $method="GET"):bool` This method is not tested at all. does not check dynamic routes.  
- `public function separate_optional_from_decoded_pattern($original_parsed)` Facilitates optional paramaters  
  
Processes a parsed pattern into an array of valid parsed patterns where the original pattern may contain details for optional paramaters  
  
- `public function addRouter(callable $router)` add a callable as a router. It will be called with a `\Lia\Obj\Request` object as the only paramater  
- `public function route(\Lia\Obj\Request $request)` get a route for the given request  
  
- `public function addDirectoryRoutes(\Lia\Package $package, string $directory, string $base_url = '/', array $exts_to_remove[".php"])`   
- `public function decode_pattern($pattern)`  Convert a pattern into a decoded array of information about that pattern  
  
The patterns apply both for the `public` dir and by adding routes via `\Lia\Addon\Router::from($lia)->addRoute()`. The file extension (for .php) is removed prior to calling decode_pattern()  
The delimiters can be changed globally by setting $router->varDelims  
  
## Examples:   
- /blog/{category}/{post} is valid for url /blog/black-lives/matter  
- /blog/{category}.{post}/ is valid for url /blog/environment.zero-waste/  
- /blog/{category}{post}/ is valid for url /blog/{category}{post}/ and has NO dynamic paramaters  
- /blog/{category}/@GET.{post}/ is valid for GET /blog/kindness/is-awesome/ but not for POST request  
- /@POST.dir/sub/@GET.file/ is valid for both POST /dir/sub/file/ and GET /dir/sub/file/  
  
## Methods: @POST, @GET, @PUT, @DELETE, @OPTIONS, @TRACE, @HEAD, @CONNECT  
- We do not currently check the name of the method, just @ABCDEF for length 3-7  
- These must appear after a `/` or after another '@METHOD.' or they will be taken literally  
- lower case is not valid  
- Each method MUST be followed by a period (.)  
  
## Paramaters:  
- NEW: Dynamic portions may be separated by by (-) and/or (:)   
- {under_scoreCamel} specifies a named, dynamic paramater  
- {param} must be surrounded by path delimiters (/) OR periods (.) which will be literal characters in the url  
- {param} MAY be at the end of a pattern with no trailing delimiter  
  
## TODO  
- {paramName:regex} would specify a dynamic portion of a url that MUST match the given regex.   
    - Not currently implemented  
- {?param} would specify a paramater that is optional  
    - Not currently implemented  
  
- `public function extract_url_paramaters($decoded_pattern, $url)` Given a url and array of paramaters,   
  
- `public function url_to_regex($url)` convert an actual url into regex that can be used to match the test regs.  
  
  
