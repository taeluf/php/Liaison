<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/addon/Error.php  
  
# class Lia\Addon\Error  
How will this be used?  
Show error at top of current page  
Redirect to error page with (or without) a message  
Show error page instead of requested page (no redirect)  
Log an error  
See source code at [/code/addon/Error.php](/code/addon/Error.php)  
  
## Constants  
  
## Properties  
- `public string $fqn = 'lia:server.error';`   
- `public $headerError = false;`   
- `protected int $statusCode = null;`   
- `public string $pageMessage = null;`   
- `public string $headerMessage = null;`   
- `public string $gotoMessage = null;`   
- `protected bool $isPageError = false;`   
  
## Methods   
- `public function init_lia()`   
- `public function onPackageReady()`   
- `public function error_goto(string $message, int $statusCode=500)`   
- `public function error_log(string $message, int $level)`   
- `public function error_header(string $message, int $statusCode=null)`   
- `public function error_page(string $message, int $statusCode=500)`   
- `public function onResponseReady(\Lia\Obj\Response $response)`   
- `public function statusCodeText(int $statusCode)`   
  
