<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/addon/Autoload.php  
  
# class Lia\Addon\Autoload  
  
See source code at [/code/addon/Autoload.php](/code/addon/Autoload.php)  
  
## Constants  
  
## Properties  
- `public string $fqn = 'lia:server.autoload';`   
- `public $dirs;` an key=>value array  
  
## Methods   
- `public function init_lia()`   
- `public function enable()`   
- `public function addDir($dir,...$baseNamespaces)` add a directory to be autoloaded, psr4-ish  
- `--declaration-missing--`   
  
