<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/addon/Server.php  
  
# class Lia\Addon\Server  
A very bad integration of the Router & other addons & utility classes that makes it easy to send a response  
  
See source code at [/code/addon/Server.php](/code/addon/Server.php)  
  
## Constants  
  
## Properties  
- `public string $fqn = 'lia:server.server';`   
- `public $bufferResponse = true;` true to flush the output buffer after sending headers & echoing response  
- `public bool $useTheme = true;` true to enable theme  
false to disable theme  
  
- `public string $themeName = 'theme';` name of a theme in `theme` or `view` dir  
  
## Methods   
- `public function init_lia()`   
- `public function setTheme($name)` Set theme to use in a web response  
  
- `public function protocol()`   
- `public function urlWithDomain($relativeUrl)`   
- `public function getResponse($url=null, $methodnull)`   
- `public function apply_theme(\Lia\Obj\Route $route,\Lia\Obj\Response $response)` Applies the theme (if response->useTheme & server->useTheme are true)  
- `public function process_route(\Lia\Obj\Route $route,\Lia\Obj\Response $response)` Process a route & setup the response's content & headers  
- `public function try_redirect_to_corrected_url($url, $method)` This is a sloppy bad function that needs rewritten  
  
- `public function send_response($response)`   
- `public function deliver($url=null, $methodnull)`   
- `protected function closeConnection()` Close the connection with client. May not work on all hosts, due to apache/server configuration, I'm pretty sure.  
  
- `public function requirePhpFileRoute(\Lia\Obj\Route $route,\Lia\Obj\Response $response)`   
- `public function getDistinctRoute($routeList)`   
  
