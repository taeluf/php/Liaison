<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/addon/View.php  
  
# class Lia\Addon\View  
  
See source code at [/code/addon/View.php](/code/addon/View.php)  
  
## Constants  
  
## Properties  
- `public string $fqn = 'lia:server.view';`   
- `public string $name = 'view';`   
- `public $views = [null=>[]];` Views to be loaded.  
- `public array $packages = [];` a lazy way to map viewname => package  
- `public array $globalArgs = [];` Args to pass to every view  
  
## Methods   
- `public function init_lia()`   
- `public function getHeadHtml()`   
- `public function addDir(string $dir, \Lia\Package $package)`   
- `public function addView(string $view_name,string $dir)`   
- `public function addViewFile(string $view_name, string $file)`   
- `public function addViewCallable(string $view_name, mixed $callable)`   
- `public function view(string $name, array $args=[])`   
- `public function show_file($view, $args)`   
- `public function show_callable($view, $args)`   
- `public function show_main($view, $args)`   
- `public function addFiles($dir)` add resource files in the given directory  
- `public function change_namespace(string $original_namespace, string $new_namespace, bool $allow_overrides = false)` Rename the original_namespace to the new_namespace. The old namespace is removed.  
Only affects views already added.   
  
