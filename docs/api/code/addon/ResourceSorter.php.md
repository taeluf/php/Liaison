<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/addon/ResourceSorter.php  
  
# class Lia\Addon\ResourceSorter  
  
See source code at [/code/addon/ResourceSorter.php](/code/addon/ResourceSorter.php)  
  
## Constants  
  
## Properties  
- `public string $fqn = 'lia:server.resourcesorter';`   
- `protected $didSet=false;`   
- `protected $orders=['css'=>[], 'js'=>[]];`   
  
## Methods   
- `public function init_lia()`   
- `public function setResourceOrder($ext,$names, $prepend=true)` Give an array of file names in the order they should be sent to the browser.  
  
- `public function getSortedFiles($from_unsorted)`   
  
