<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/addon/Cache.php  
  
# class Lia\Addon\Cache  
  
See source code at [/code/addon/Cache.php](/code/addon/Cache.php)  
  
## Constants  
  
## Properties  
- `public string $fqn = 'lia:server.cache';`   
- `public $dir;` The dir to write cache files to  
- `public bool $enabled = true;`   
- `public int $stale_check_frequency = 60*24;` Number of minutes between stale cache checks  
- `public $main = 'main.php';` The main file to write to & read from  
- `public array $props = [];` Key=>values that get written to disk in a single file (call $cache->write())  
  
## Methods   
- `public function __construct($package=null, $namenull)`   
- `public function init_lia()`   
- `public function main_file_path()`   
- `public function read()` reads main key=>value pairs from disk into memory  
- `public function write()` Write main key=>value pairs to disk from memory  
- `public function is_cache_file_valid($key)` Check if: cache is enabled, cached file exists, cached file is NOT expired  
- `public function get_cache_file_content($key)` Get content of a cached file, if cache is valid  
- `public function get_cache_file_path($key)` Get path to cached file if cache is valid  
- `public function cache_file_path($key)` Get the path for a cached file  
  
- `public function cache_meta_file_path($key)` get the path to a meta file, which contains expiration time of cached files  
- `public function cache_file($key, $value, $maxAge=60*60*24*5)` Store $value to file by identified by $key  
- `public function delete_stale_files()` Delete stale cache files IF a stale cache check is required  
- `public function run_delete_stale_files()` Delete stale cache files (unconditional)  
- `public function delete_all_cache_files()` Deletes ALL cache files, whether expired or not  
- `public function is_stale_cache_check_required()` check if stale cache check is required  
- `protected function deleteStaleCacheFiles()`   
- `public function create_dir($dir)` Creates directory if it does not exist.   
- `public function set($key, $value)`   
- `public function get($key)`   
- `public function __set($key,$value)`   
- `public function __get($key)`   
  
