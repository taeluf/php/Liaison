<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/addon/Seo.php  
  
# class Lia\Addon\Seo  
Resources for needed seo data/  
https://developer.twitter.com/en/docs/twitter-for-websites/cards/guides/getting-started  
https://ogp.me/  
https://www.w3schools.com/tags/tag_meta.asp  
NOT USED https://schema.org  
See source code at [/code/addon/Seo.php](/code/addon/Seo.php)  
  
## Constants  
  
## Properties  
- `public string $fqn = 'lia:server.seo';`   
- `public $seo = [];` array of seo values like:  
`['title' => 'The Title', 'description'=>'the description', ...]`  
- `public $html = [];` array of html like `['title'=>['<title>The Title</title>', '<meta ...'], 'image'=>['<meta property="og:image:alt" ...>']]`  
  
## Methods   
- `public function init_lia()`   
- `public function addHeadHtml($html)`   
- `public function seo($params)` Shorthand to set seo paramaters from an array.   
  
- `public function title($title)`   
- `public function description($description)`   
- `public function meta(string $name, string $content)` Add any meta tag  
- `public function image($url, $altText)`   
- `public function url($url)`   
- `public function site_name($siteName)`   
- `public function keywords($keywords)`   
- `public function get_html()`   
  
