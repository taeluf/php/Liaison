<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/addon/Resources.php  
  
# class Lia\Addon\Resources  
A component for managing multiple css & js files for a single request.  
  
  
See source code at [/code/addon/Resources.php](/code/addon/Resources.php)  
  
## Constants  
  
## Properties  
- `public string $fqn = 'lia:server.resources';`   
- `public $files = [];`   
- `public $urls = [];`   
- `public $sorter = [];`   
- `public $cache;` an instance of Lia\Addon\Cache  
- `public $minify = true;` to minify css & js or not  
- `public $useCache = true;` Whether to enable cache or not.  
  
  
## Methods   
- `public function __construct(\Lia\Package $package=null)`   
- `public function init_lia()`   
- `public function onPackageReady()`   
- `protected function cleanExt($ext)`   
- `protected function cacheMapName($ext)`   
- `public function setSorter(string $ext, mixed $sorter)` Set a function/callable to be used for sorting resource files before they are output to a batch file. You can also use this to remove files from the list.  
  
- `public function addFile(string $file)` Add a css or js file to be added to the final output  
  
- `public function addUrl($url)`   
- `public function getHtml(): string` Get `<head>` html with javascript and css tags, and seo-related meta tags.  
- `public function getFileTag($ext)`   
- `public function getUrlTag($ext)`   
- `public function getSortedFiles($ext)`   
- `public function concatenateFiles($ext)`   
- `public function concatenateFileNames($ext)`   
- `public function compileFilesToCache($ext)`   
- `public function getCompiledFilesUrl($ext)`   
- `public function minifyFiles(string $fileContent, string $ext)`   
  
