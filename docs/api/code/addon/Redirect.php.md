<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/addon/Redirect.php  
  
# class Lia\Addon\Redirect  
  
See source code at [/code/addon/Redirect.php](/code/addon/Redirect.php)  
  
## Constants  
  
## Properties  
- `public string $fqn = 'lia:server.redirect';`   
- `public $redirectUrl;`   
- `public $withMessage = false;`   
- `public $statusCode = 302;`   
- `public $message = null;`   
  
## Methods   
- `public function init_lia()`   
- `public function goto($url, $statusCode=302)`   
- `public function gotoSetMessage($message)`   
- `public function gotoWithMessage($url, $message, $statusCode=302)`   
- `public function getGotoMessage()` Get the redirect message associated with `$_GET['message_id']`  
    
- `public function get_url()` get the url with `message_id` set in query params.  
Will cache redirect message to a file if one is present  
  
- `public function go()`   
  
