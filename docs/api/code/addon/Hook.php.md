<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/addon/Hook.php  
  
# class Lia\Addon\Hook  
  
See source code at [/code/addon/Hook.php](/code/addon/Hook.php)  
  
## Constants  
  
## Properties  
- `public string $fqn = 'lia:server.hook';`   
- `public $hooks = [];` array of hooks like `['hook.name'=>[ [$object, 'method'], function(){echo "one";} ] ];`  
  
## Methods   
- `public function init_lia()`   
- `public function prefix_method_added($obj, $method, $hook_name)` Add an object's method as a hook.   
- `public function add($name, $callable)`   
- `public function call($name, ...$args)`   
  
