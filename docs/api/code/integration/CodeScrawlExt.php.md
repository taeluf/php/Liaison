<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/integration/CodeScrawlExt.php  
  
# class Lia\Integration\CodeScrawlExt  
  
See source code at [/code/integration/CodeScrawlExt.php](/code/integration/CodeScrawlExt.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function bootstrap()`   
- `public function scrawl_finished()`   
  
