<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/unit/FastFileRouter.php  
  
# class Lia\Test\FastFileRouter  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testMakeExtMap()` used to generate a mimetype map from array into one file for each extension  
- `public function testCssFile()`   
- `public function testGetParams()`   
- `public function testEndingSlash()`   
- `public function testDotDot()`   
- `public function testFastRoute()`   
  
