<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Addon/Router.Other.php  
  
# class Liaison\Test\Addon\RouterOther  
Old router tests that are sloppy, confusing, hard to understand but still provide utility.  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testOptionalParamater()`   
- `public function testGetRoute()`   
- `public function testUrlToTestReg()`   
- `public function testParsePatterns()`   
  
