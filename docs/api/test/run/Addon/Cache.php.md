<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Addon/Cache.php  
  
# class Liaison\Test\Addon\Cache  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testDeleteStaleFiles()`   
- `public function testFileCache()`   
- `public function testMainCacheViaLiaMethod()` Tests \Lia\Addon\Cache::from($lia)->set() method & \Lia\Addon\Cache::from($lia)->get() method  
- `public function testMainCacheViaLiaSet()`   
- `public function testMainCache()` Uses $cache->set('a.key') & $cache->get('a.key');  
- `protected function cache_dir()`   
  
