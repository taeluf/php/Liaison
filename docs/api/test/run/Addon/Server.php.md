<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Addon/Server.php  
  
# class Liaison\Test\Addon\Server  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testResolveConflictingRoutes()`   
- `public function testSetTheme()`   
- `public function testResponseContentNoTheme()`   
- `public function testResponseContentWithTheme()`   
- `public function theme_view($name, $args)`   
- `public function sample_content($route, $response)`   
  
