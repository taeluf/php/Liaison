<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Addon/View.php  
  
# class Liaison\Test\Addon\View  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testNestedViewResources()`   
- `public function testSiblingViewResources()`   
- `public function testAddViewMain()`   
- `public function testAddViewCallable()`   
- `public function testAddViewFile()`   
- `public function view_dir()`   
- `public function view_dir_2()`   
- `public function testViewNotFound()`   
- `public function testNamespaceNotFound()`   
- `public function testNoNamespaceFallback()`   
- `public function testAddViewCallableWithNamespace()`   
- `public function testAddViewCallable2()`   
  
