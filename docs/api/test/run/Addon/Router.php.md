<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Addon/Router.php  
  
# class Liaison\Test\Addon\Router  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testPatternAndParamatersToUrl()`   
- `public function testExtractUrlParamaters()`   
- `public function testDecodePatternWithOptionalParamaters()`   
- `public function testUrlToRegex()` Example of `$router->url_to_regex()`  
- `public function testDecodePattern()` Example of `$router->decode_pattern()`  
- `public function testDecodedPatternToUrl()` Example of `$router->decoded_pattern_to_url()`. @see(testParsePatternExample) to see what the `$decoded` array looks like  
- `public function testRemoveRoute()`   
- `public function testRemoveReplaceRoute()`   
  
