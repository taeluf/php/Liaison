<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Addon/Resources.php  
  
# class Lia\Test\Addon\Resources  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testAddCssAndJavascriptFiles()` for documentation generation purposes  
- `public function testAddUrl()`   
- `public function testCompiledHtml()`   
- `public function testDuplicateCompile()`   
- `public function testDeliverCss()`   
- `public function testDeliverJs()`   
- `public function testCacheCompiledJSFile()`   
- `public function testCacheCompiledCSSFile()`   
- `public function testSortCss()`   
- `public function testRemoveCssFile()` Test that the Resource Sorter is able to REMOVE a css file from the list.  
- `public function testCompileCss()`   
- `public function testSortJs()`   
- `public function testCompileJs()`   
- `protected function dir()`   
  
