<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Other/FancyClosure.php  
  
# class Liaison\Test\FancyClosure  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function retBoundArg($boundArg, $passedArg)` placeholder docblock  
- `public function testObjectCallable()`   
- `public function testStaticCallable()`   
- `public function testAnonymousCallable()`   
  
