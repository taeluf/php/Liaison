<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Other/Server.php  
  
# class Tlf\Tester\Test\Server  
Test the fully-functional server  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testDynamicUrl()`   
- `public function testUsesCorrectTheme()`   
- `public function testCachedCssFile()`   
- `public function testStaticFile()`   
- `public function testServer()`   
  
