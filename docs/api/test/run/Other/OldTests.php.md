<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Other/OldTests.php  
  
# class Lia\Test\OldTests  
To keep old tests that I used to help write features.  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testPrefixScanning()` The flow of how scanning & prefixes work. This is all abstracted away now and internals can change.  
  
This test will not be maintained  
  
