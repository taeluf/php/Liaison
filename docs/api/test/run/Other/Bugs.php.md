<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Other/Bugs.php  
  
# class Lia\Test\Bugs  
for testing bugs I find while working on Liaison  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testViewLiaObj()`   
- `public function testViewAddsChildResources()` Happening on taeluf.com. I have a `theme.php` view & associated `.css` and `.js` files in `theme/` dir. I think the files are all being added to `resources` addon, BUT they're NOT being output to the cached css file  
  
This test FAILED to re-create the bug. So I realized that resource files were being added AFTER the theme view was being `require`d. I moved the code around & now it works. I have not added a test for this.  
  
  
