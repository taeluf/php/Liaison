<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# SEO - Search Engine Optimization  
The Seo addon simplifies the addition of meta tags that search engines and social media sites need, like the `og:image` and `og:description` tags, as well as the `<title>` on the page. Any other HTML can be also be added via this addon to the `<head>` of the page.   
  
The Resources addon produces its own `<head>` html for scripts and stylesheets and collects html from the SEO addon. The Theme view will get this html from the resources addon and print it.  
  
**Related:** [Resources](/docs/Resources.md), [Views](/docs/Views.md), [Seo Addon](/docs/api/code/addon/Seo.php.md)  
  
## Docs  
- Convenience Methods  
- Seo tags from array  
- Arbitrary `<head>` html  
  
## Convenience Methods  
Call any or all of the following methods to setup the meta tags for a page. Be sure to use full URLs, as some search engines and social media sites require this.  
```php  
<?php  
$seo = \Lia\Addon\Seo::from($lia);  
$seo->title('Test Page'); // <title> and og:title  
$seo->description('Test description'); // <meta name="description">, og:title, twitter:card summary, and og:type article  
$seo->image('https://example.com/path/to/image.jpg', 'required alt text for image'); // og:image, twitter:image, og:image:alt  
$seo->url('https://example.com/canonical/url/'); // link canonical, og:url  
$seo->site_name('Liaison test'); // og:site_name  
$seo->keywords('blog,politics,fascism'); // meta name="keywords"  
```  
  
**Output:**  
```html  
<title>Test Page</title>  
<meta property="og:title" content="Test Page" />  
<meta name="description" content="Test description" />  
<meta property="og:description" content="Test description" />  
<meta property="og:type" content="article" />  
<meta property="twitter:card" content="summary" />  
<meta property="og:image" content="https://example.com/path/to/image.jpg" />  
<meta property="twitter:image" content="https://example.com/path/to/image.jpg" />  
<meta property="og:image:alt" content="required alt text for image" />  
<link rel="canonical" href="https://example.com/canonical/url/" />  
<meta name="og:url" content="https://example.com/canonical/url/" />  
<meta name="og:site_name" content="Liaison test" />  
<meta name="keywords" content="blog,politics,fascism" />  
```  
  
## Seo tags from array  
**From Array:**  
You can also setup seo data from an array, which will call each of the appropriate methods above. (*the array key is the method name*)  
```php  
<?php  
$seo = \Lia\Addon\Seo::from($lia);  
$seo_data = [  
    'title' => 'Test Page',  
    'description' => 'Test description',  
    'image' => ['https://example.com/path/to/image.jpg', 'required alt text for image'],  
    'url' => 'https://example.com/canonical/url/',  
    'site_name' => 'Liaison test',  
    'keywords'=> 'blog,politics,fascism,2025',  
];  
$seo->seo($seo_data);  
```  
  
The output is the same as calling the methods directly.  
  
## Arbitrary HTML & Meta tags  
- `\Lia\Addon\Seo::from($lia)->addHeadHtml('<arbitrary html>');` - add any html you want to the `<head>`  
- `\Lia\Addon\Seo::from($lia)->meta("example", "information");` - Add an arbitrary `<meta>` tag to the `<head>` like `<meta name="example" content="information">`  
