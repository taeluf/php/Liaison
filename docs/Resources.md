<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Resources (css & js)  
The Resources Addon manages css & javascript files, concatenating them together into a single `.css` or `.js` file. Views work automatically with the Resources addon to facilitate this. Your Theme will get HTML from the Resources addon to print in the `<head>`, which will include script and stylesheet tags, as well as SEO meta tags from the SEO addon. Any `<head>` html can be added dynamically to the `SEO` addon to be returned by the `Resources` addon.  
  
You can add, remove, and sort the lists of files manually as needed. The concatenated files are cached. You can also add files as routes and add them via URL to the Resources Addon or directly to the markup as HTML.  
  
**Related:** [Views](/docs/Views.md), [Theme](/docs/Theme.md), [SEO](/docs/Seo.md), [Resources Addon](/docs/api/code/addon/Resources.php.md),  [ResourceSorter Addon](/docs/api/code/addon/ResourceSorter.php.md), [Cache](/docs/Cache.md), [Routes](/docs/Routes.md)  
  
## Docs  
- Add Files  
- Sort files with the Resource Sorter addon   
- Sort files manually  
- Remove files  
- Print `<head>` html  
- Sensitive Data (Cache & Routing)  
  
## Add Files  
For automatically adding resource files alongside views, see [Views](/docs/Views.md).   
  
Otherwise:   
```php  
<?php  
$resources = \Lia\Addon\Resources::from($lia); // your Liaison instance  
  
// Files will be concatenated and added to a singular css file  
$private_dir = dirname(__DIR__,2).'/input/private/resources/';  
$resources->addFile($private_dir.'/undeliverable-styles.css');  
$resources->addFile($private_dir.'/undeliverable-scripts.js');  
  
// URLs will be added as `<link>` and `<script>` tags and added to the HTML head  
$resources->addURL('/resources/page-layout.css'); // deliverable on your site  
$resources->addURL('https://cdn.example.com/my-namespace/dancing-bear.js'); // external file  
```  
  
## Sort files via relative paths  
This allows you to sort css or javsacript files by providing only relative paths, so that CSS rules and js scripts are loaded in the desired order.   
  
```php  
<?php  
\Lia\Addon\ResourceSorter::from($lia)  
    ->setResourceOrder(  
        $extension='css', // js for javascript files  
        $relative_file_paths= // an array of relative file paths, in the order you want them. There MUST NOT be leading `/`. There MAY be as many `/` as you like  
            ['one.css',   
            'two.css',   
            'three.css',   
            'four.css',  
            ],  
        $prepend=false // FALSE will put these names, in this order, at the end of the list of all css files. TRUE will put them at the beginning in this order.   
    );  
```  
  
  
## Sort files via absolute paths  
This way, you must sort using absolute paths, or implement your own relative-path solution.  
  
```php  
<?php   
\Lia\Addon\Resources::from($lia)->setSorter('css',  
    /** $files is an array of absolute paths. */  
    function(array $files): array{  
        $files = array_values($files);  
        return  
        [  
            $files[1], // b.css  
            $files[0], //a.css  
            $files[2], //c.css  
        ];  
    }  
);  
```  
  
  
## Remove files  
Use the sorter mechanism to remove files. Just remove them from the array you return.  
  
```php  
<?php   
\Lia\Addon\Resources::from($lia)->setSorter('css',  
    /** $files is an array of absolute paths. */  
    function(array $files): array{  
        $files = array_values($files);  
        return  
        [  
            // $files[0], //a.css is removed from the list.  
            $files[1], // b.css  
            $files[2], //c.css  
        ];  
    }  
);  
```  
  
## Print `<head>` html  
To add arbitrary html to the `<head`> see the SEO addon.  
  
Print script and stylesheet tags and SEO meta tags by calling:   
```php  
<?php  
echo \Lia\Addon\Resources::from($lia)  
    ->getHtml();  
```  
  
Typically, this will be called within your Theme View's `<head>`.  
  
## Sensitive Data (Cache & Routing)  
2025-02-11:    
The Resources addon concatenates together CSS & Javsacript files for a request, then caches them into the `cache-resources` directoy of the built-in liaison package. (*i.e. `vendor/taeluf/liaison/code/cache-resources/`*). During the `onPackageReady()` of the Resources addon, the FastFileRouter is used to deliver these files statically. You cannot protect access to any of these cached files.  
  
This could be a problem if you put any sensitive data (*such as API keys*) in a javascript file that is only delivered on admin pages. In these cases, it would be best to have a protected URL for this javascript, then to add the URL to the head, rather than add the file to the concatenated list.  
  
I don't currently have plans to change this implementation.   
