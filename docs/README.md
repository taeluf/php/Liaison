<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Liaison  
Liaison is a web-framework for building portable web-app libraries and fully-functioning websites.  
    
**Under Development!** Some breaking changes are underway, but if it's documented, you should be able to count on it.  
  
## Install  
```bash  
composer require taeluf/liaison v0.7.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/liaison": "v0.7.x-dev"}}  
```  
  
  
## Documentation  
**Notice:** A documentation rewrite/reorganization is in-progress. Some of these links are broken. Old documentation is below.  
- Getting Started (*below*): Setup a Liaison-Powered Web Server  
- [Packages](/docs/Packages.md): Contains your routes, views, theme, addons, hooks, and more.  
- TODO [Addons](/docs/Addons.md): Organize features that you're adding to Liaison, whether for your own site, or for a library you're building.  
- [Routes](/docs/Routes.md): Files in your `public/` dir are deliverable. Additional routing options available.  
- [Views](/docs/Views.md): Re-usable HTML/CSS/Javascript components.  
- [Theme](/docs/Theme.md): Your site's HTML page layout is just another view.   
- [Resources (CSS & Javascript](/docs/Resources.md)): Add, remove, and manage css & javascript files on pages.  
- [Hooks](/docs/Hooks.md): Respond to events like `REQUEST_STARTED` and `VIEW_LOADED`.  
- [SEO](/docs/Seo.md): Add `<head>` markup for search engines and social media sites.  
- [Markdown Support](/docs/Markdown.md): Markdown support can be added using Hooks.  
- TODO [Server](/docs/Server.md): Puts together a request/response, defining hooks, loading the theme, and more.  
- TODO [Cache](/docs/Cache.md): Cache files to save computation  
- [Notes](/docs/Notes.md): `@NOTE`s throughout the library.  
  
## Getting Started  
Liaison (*`class Lia`*) ties together multiple different Packages (*`class Lia\Package`*). Packages contain addons (*`class Lia\Addon`*), views (*php scripts*), public files (*php scripts*), and anything else that is part of your package.  
  
Liaison provides a built-in Server package with addons for routing, hooks, cache, SEO head tags, CSS & Javascript bundling, and views. There is also a `Server` addon that processes requests.  
  
I recommend using a main `Site` package as the base of your site, then create additional packages for any complex features, or features you might want to use on another website later.  
  
### Create a Web Server  
`php -S localhost:3000 deliver.php` to run the below server.    
  
`/deliver.php`:  
```php  
<?php  
require_once(__DIR__.'/vendor/autoload.php');  
  
$lia = new \Lia();    
  
// Add the built-in Package, which provides all the web-server features.    
$server_package = new \Lia\Package\Server($lia, $fqn='lia:server');  // dir & base_url not required    
  
// Create your site's package, containing your routes, views, theme, addons, hooks, and more  
$site_package = new \Lia\Package\Server($lia, 'vendor:namespace', __DIR__.'/Site/');    
  
  
\Lia\Addon\Server::from($lia)->deliver();    
```  
  
### Create the home page  
Create the following file, then visit `http://localhost:3000/` in your browser.  
  
`/Site/public/index.php`  
```php  
<h1>Hello Liaison</h1>  
<p>This is my first Liaison-powered home page!</p>  
```  
  
### What's Next?  
Create a theme for your page layout, then start building out your site. See the Documentation links above to learn more about each feature. Start with public files, hooks, and views. Addons and Packages are a bit more advanced.  
  
  
---  
  
## Old Docs  
I'm working on rewriting and reorganizing the documentation. Below is all the documentation that has not yet been reorganized.  
  
  
### Addon Classes  
An Addon subclass should provide specific functionality to developers using your package, or provide features within your own package, or provide features to website visitors.  
  
Minimal Example:  
```php  
<?php  
  
namespace ReedyBear\Example;  
  
class MyStupidAddon extends \Lia\Addon {  
  
    public function __construct(?\Lia\Package $package=null){  
        parent::__construct($package);  
        // most setup should probably go in `init_lia()` or `onPackageReady()`... idk.  
    }  
  
    /**   
    * Called when Liaison has been setup for the package, but the Package is not fully ready yet.  
    *   
    * For `\Lia\Package` subclasses, call `$package->init_lia()` to invoke every addon's `init_lia($package)` method.  
    * For `\Lia\Package\Server` subclasses, `$package->init_lia()` is automatically called at the end of `__construct()` just before `onPackageReady()` will be called.  
    *   
    */  
    // public function init_lia(\Lia\Package $package): void{}  
    public function init_lia(){} // this old method signature will be fixed, as the commented one above is how it is called.  
  
    /**  
     * Called when the package is fully setup, for addons to do any final setup steps.  
     *  
     * For `\Lia\Package` subclasses, call `$package->ready()` to invoke every addon's `onPackageReady($package)` method.  
     * For `\Lia\Package\Server` subclasses, `$package->ready()` is automatically called at the VERY end of `__construct()`, AFTER `init_lia($package)` has been called on every addon.  
     */  
    // public function onPackageReady(\Lia\Package $package): void{}   
    public function onPackageReady(){} // this old method signature will be fixed, as the commented one above is how it is called.  
}  
```  
  
## Notes/Tips/Questions  
See [Notes](/docs/Notes.md) for various undocumented functionality.  
  
### Planned Changes  
These changes will take place piece-by-piece over time. I plan no major overhauls in a single update.  
- Hooks: Many hooks defined within this repo are strings like `'RequestStarted'` and they're hardcoded where the hook is called. I'm slowly-but-surely converting these to class consts, like `\Lia\Hooks::REQUEST_STARTED = 'lia:server.request_started'`.   
- Types: Much of Liaison does not have types hardcoded. This was a 'feature' previously, but I hate it. I'm adding hardcoded types slowy-but-surely.  
- Methods: I've considered removing global methods and adding some new methods to `Lia` itself. I've also considered creating a compiled version of `Lia`, but I don't think I'll ever do that. Calling methods directly may break at some point, in favor of load-addon, call addon.  
  
### Ideas  
- `$lia->depend('ns:package)` or `$lia->depend('ns:package.addon')` as a way to formalize dependencies, throwing an exception if the dependency is not present. Might be nicer than the if has approach.  
  
### For Documentation  
- `config.json` package settings is not documented at all. I don't know what goes in that file or how to use it in my package.  
- Write: Addons are meant to organize specific features, like routing or views. Packages are meant to organize sets of addons that work together.  
